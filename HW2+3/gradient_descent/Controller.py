import numpy as np 
from collections import defaultdict
import pdb

def shuffle_pair(a, b):
	assert len(a) == len(b)
	idxs = np.random.permutation(len(a))
	return a[idxs], b[idxs]


class DFBuilder():
	def __init__(self):
		pass
	def loss(self, loss_op):
		self.loss = loss_op
		return self
	def input(self, inp):
		self.inp = inp	
		return self
	def output(self, output):
		self.out = output
		return self
	def target(self, target):
		self.tar = target
		return self
	def compile(self):
		try:
			return Differentiable(self.loss, self.inp, self.out, self.tar)
		except AttributeError:
			raise Exception("Cannot compile a model without specifying its input, output, target, and loss operations")

class Differentiable():
	def __init__(self, loss, inp, out, tar):
		self.loss = loss
		self.input = inp
		self.output = out
		self.target = tar
		self.parameters = loss.get_parameters()

	def gradient_descent(self, train_x, train_y, n_iter=1000, learn_rate=0.1):
		shuffled_x, shuffled_y = shuffle_pair(train_x, train_y)
		for idx in range(n_iter):
			x, y = shuffled_x[idx % len(shuffled_x)], shuffled_y[idx % len(shuffled_y)]
			grads = {}
			for param in self.parameters:
				grads[param.name] = self.loss.grad_wrt(param, {self.input.name : x, self.target.name : y})
			# these loops must be separate to avoid changing upstream weights before evaluating downstream gradients	
			for param in self.parameters:
				param -= learn_rate * grads[param.name]

	def newton_method(self, train_x, train_y, n_iter=15):
		shuffled_x, shuffled_y = shuffle_pair(train_x, train_y)
		symbolic_grads_1 = { param.name : self.loss.symbolic_grad_wrt(param) for param in self.parameters }
		symbolic_grads_2 = { param.name : symbolic_grads_1[param.name].symbolic_grad_wrt(param) for param in self.parameters }
		for idx in range(n_iter):
			x, y = shuffled_x[idx % len(shuffled_x)], train_y[idx % len(train_y)]
			update = {}
			for param in self.parameters: 
				first_der = symbolic_grads_1[param.name].eval({self.input.name : x, self.target.name : y})
				second_der = symbolic_grads_2[param.name].eval({self.input.name : x, self.target.name : y})
				if((second_der != 0).all()):
					update[param.name] =  first_der / second_der
				else:
					pdb.set_trace()
					update[param.name] = 0
			# these loops must be separate to avoid changing upstream weights before evaluating downstream gradients
			for param in self.parameters:
				param -= update[param.name]
	def eval(self, x_batch):
		return np.array([self.output.eval({self.input.name : x}) for x in x_batch])
	def sum_loss(self, x_batch, y_batch):
		loss = 0
		for x, y in zip(x_batch, y_batch):
			loss += np.absolute(self.loss.eval({self.input.name: x, self.target.name: y}))
		return loss
	def mean_loss(self, x_batch, y_batch):
		return self.sum_loss(x_batch, y_batch) / len(x_batch)
	