import numpy as np
# This is a stub file for easily loading data


def load_spambase():
	'''
	Returns (x, y) where x is a matrix of observations and y is a vector of labels
	'''
	data = np.genfromtxt("data/spambase.data", delimiter=",")
	x = data[:, :-1]
	y = data[:, -1]
	return (x, y)

def load_housing():
	'''
	Returns ((train_x, train_y), (test_x, test_y)) where each x is a matrix of observations and each y is a vector of labels
	'''
	train_data = np.genfromtxt("data/housing_train.txt")
	train_x = train_data[:, :-1]
	train_y = train_data[:, -1]

	test_data = np.genfromtxt("data/housing_test.txt")
	test_x = test_data[:, :-1]
	test_y = test_data[:, -1]

	return ((train_x, train_y), (test_x, test_y))

def shift_scale(observations, minim=0, maxim=1):
	'''
	Scales every column of observations to have the given min and max values

	Parameters:
	----------
	observations: a matrix of observations
	min: minimum value
	max: maximum value
	'''

	# to [0, 1]
	observations = (observations - observations.min(0)) / observations.ptp(0) 

	# to desired
	observations = (maxim - minim) * observations + minim
	return observations

def avg_normalization(observations):
	'''
	Subtracts the mean from every column and divides by the range

	Parameters:
	----------
	observations: a matrix of observations
	'''
	return (observations - np.mean(observations, axis=0)) / observations.ptp(0)

def add_bias_dim(observations):
	'''
	Adds a column of ones as the new first column in observations

	Parameters:
	----------
	observations: a matrix of observations
	'''
	return np.concatenate((np.ones((observations.shape[0], 1)), observations), 1)