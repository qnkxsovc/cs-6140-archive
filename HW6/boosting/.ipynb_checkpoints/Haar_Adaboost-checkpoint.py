from collections import defaultdict
import numpy as np
import math


def _hamming_distance(x, y):
	dif = np.abs(x - y)
	return np.sum(dif) / len(dif)


class ECOC():
	def __init__(self):
		self.d_encoder = -1
		self.d_decoder = -1
		self.adaboosters = -1
		self.num_labels = -1
		self.num_bits = -1
		self.bitstrings = -1
		self.int_bitstrings = -1

	def fit(self, all_x, all_y, num_stumps=100, bitstr_len=20):

		haar_idxs = [make_haar_idx(x) for x in all_x]

		labels = np.unique(all_y)
		self.num_labels = len(labels)

		self._make_coders(labels, bitstr_len)
		bitstrs = self.bitstrings
		self.num_bits = bitstr_len

		label_groups = [bitstrs[:, i][all_y] for i in
		                range(bitstr_len)]  # store groups of labels for training each adabooster

		def train_booster(args):
			x_features, y_bin, index = args
			print(index)
			y_bin[y_bin == 0] = -1
			a = HaarAdaboost()
			a.fit(x_features, y_bin, num_stumps, num_stumps, haar_idxs = haar_idxs)
			return (a, index)

		batch_args = [(all_x, label_groups[i], i) for i in range(bitstr_len)]

		adaboosters = map(train_booster, batch_args)
		self.adaboosters = {bt: booster for booster, bt in adaboosters}

	def _make_coders(self, labels, codeword_size):
		# goal: generate len(labels) number of codewords each with codeword_size bits
		# do so by randomly selecting binary strings without replacement
		bit_strs = np.random.randint(0, 2, size=(len(labels), codeword_size))
		int_strs = [sum([row[i] * 2 ** i for i in range(len(row))]) for row in bit_strs]
		self.d_encoder = {idx: bit_strs[idx, :] for idx in range(len(labels))}
		self.d_decoder = {int_strs[idx]: idx for idx in range(len(labels))}
		self.bitstrings = bit_strs
		self.int_bitstrings = int_strs

	def batch_accuracy(self, x, y):
		self._assert_fit()
		num_correct = 0
		for x_ins, y_ins in zip(x, y):
			num_correct += int((self.classify(x_ins) == y_ins).all())
		return num_correct / len(x)

	def classify(self, x):
		self._assert_fit()

		raw_classification = np.array([self.adaboosters[idx].classify(x) for idx in range(self.num_bits)])
		raw_classification[raw_classification == -1] = 0

		best_hd = math.inf
		best_bitstr = -1
		for bitstr in self.bitstrings:
			hd = _hamming_distance(bitstr, raw_classification)
			if hd < best_hd:
				best_bitstr = bitstr
				best_hd = hd
		best_intstr = sum([best_bitstr[i] * 2 ** i for i in range(len(best_bitstr))])
		return self.d_decoder[best_intstr]

	def _assert_fit(self):
		if (self.d_encoder == -1 and self.d_decoder == -1 and self.adaboosters == -1 and self.bitstrings == -1):
			raise Exception("The model must be fit before it can be trained.")

def make_haar_idx(img):
	m, r = np.min(img), np.ptp(img)
	img = (img - m) / r
	b = defaultdict(int)

	for i in range(img.shape[0]):
		for j in range(img.shape[1]):
			pix_black = int(img[i, j] > 0.5)
			b[(i, j)] = b[(i-1, j)] + b[(i, j-1)] - b[(i-1, j-1)] + pix_black
	return dict(b)

def haar_map_lookup(mp, points):
	'''
	a . . . b
	. . . . .
	. . . . .
	c . . . d
	'''
	a, b, c, d = points
	return mp[d] - mp[c] - mp[b] + mp[a]
def eval_haar_feature(mp, feature):
	h_or_v, a, b, c, d, e, f = feature
	if(h_or_v == 1):
		# horizontal 
		if(haar_map_lookup(mp, (a, b, c, d)) - haar_map_lookup(mp, (c, d, e, f)) > 0):
			return 1
		else:
			return -1
	else:
		if(haar_map_lookup(mp, (a, c, b, d)) - haar_map_lookup(mp, (c, e, d, f)) > 0):
			return 1
		else:
			return -1

class HaarAdaboost():
	def __init__(self):
		self.alphas = -1
		self.stumps = -1
	def fit(self, all_x, all_y, num_stumps=100, bins=10, haar_idxs=None):
		if(haar_idxs == None):
			haar_idxs = [make_haar_idx(x) for x in all_x]
		im_shape = all_x[0].shape

		haar_feature = -1
		def find_haar_stump():
			# horizontal feature
			tl = (np.random.randint(0, im_shape[0] - 5), np.random.randint(0, im_shape[0] - 5))
			h, w = np.random.randint(5, im_shape[1] - tl[1]), np.random.randint(5, im_shape[0] - tl[0])
			br = (tl[0] + w, tl[1] + h)

			'''
			a . . . . . b
			. . . . . . .
			c . . . . . d
			. . . . . . .
			e . . . . . f
			'''
			a, b = tl, (br[0], tl[1])
			e, f = (tl[0], br[1]), br
			c, d = (a[0], int((a[1] + e[1])/2)), (b[0], int((b[1] + f[1])/2))
			horiz_feature = (1, a, b, c, d, e, f)

			'''
			a . c . e
			. . . . .
			. . . . .
			. . . . .
			. . . . .
			. . . . .
			b . d . f
			'''
			a_, b_ = tl, (tl[0], br[1])
			e_, f_ = (br[0], tl[1]), br
			c_, d_ = (int((a_[0] + e_[0])/2), a_[1]), (int((b_[0] + f_[0])/2), b_[1])
			vert_feature = (0, a_, b_, c_, d_, e_, f_)

			# generate horiz feature info
			haar_preds =  np.array([eval_haar_feature(haar_idxs[i], horiz_feature) for i in range(len(all_x))])
			horiz_error = np.sum((all_y != haar_preds).astype(int)) / len(all_x)
			horiz_perf = abs(1/2 - horiz_error)


			# generate vert feature info
			haar_preds =  np.array([eval_haar_feature(haar_idxs[i], vert_feature) for i in range(len(all_x))])
			vert_error = np.sum((all_y != haar_preds).astype(int)) / len(all_x)
			vert_perf = abs(1/2 - vert_error)
			return ((horiz_feature, vert_perf, vert_error), (vert_feature, vert_perf, vert_error))

		d = np.full(len(all_x), 1/len(all_x))

		self.alphas = []
		self.stumps = []

		for n in range(num_stumps):
			stumps = find_haar_stump()
			for haar_feature, perf, error in stumps:
				a_t = math.log((1 - error) / error) / 2

				signs = - (all_y * np.array([eval_haar_feature(haar_idxs[i], haar_feature) for i in range(len(all_x))]))
				upd = math.e**(signs * a_t)
				d = d*upd
				d = d / np.sum(d)
				self.alphas.append(a_t)
				self.stumps.append(haar_feature)

	def score(self, x, haar_index=None):
		self._assert_fit()
		if(haar_index == None):
			haar_index = make_haar_idx(x)
		score = np.sum([self.alphas[i] * eval_haar_feature(haar_index, self.stumps[i]) for i in range(len(self.alphas))])
		return score
	def classify(self, x):
		self._assert_fit()
		return 1 if self.score(x) > 0 else -1
	def batch_accuracy(self, x, y):
		self._assert_fit()
		num_correct = 0
		for x_ins, y_ins in zip(x, y):
			num_correct += int((self.classify(x_ins) == y_ins).all())
		return num_correct / len(x)
	def _assert_fit(self):
		if(self.alphas == -1 and self.stumps == -1):
			raise Exception("Model cannot be used before it is fit to data")

