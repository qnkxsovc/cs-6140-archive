from collections import Counter
from multiprocessing import Pool
import math
import numpy as np
import pdb 

# General algorithm 
# Questions:
#	- Classification or regression?
#	- Handling discrete vs continuous features
#	- the classification vs regression decision can be made on a per feature basis
# impurity measure can be given as an argument

# let's start with a basic model:
#	- classification tree on numeric features with entropy impurity

# Basic class structure
# DecisionTree() - would take arguments describing impurity, stop criteria, etc
# DecisionTree.fit(x, y)
# DecisionTree.predict(x)
# DecisionTree.prune() - ?

# x is a matrix where rows are observations and columns are features
# Each column should have discrete values
# y is a column vector where each row contains the label for rows in x

class Presorter():
	def __init__(self, x, y):
		self.x = x
		self.y = y
		self.sorted_idxs = np.argsort(x, axis=0)
		self.reverse_idxs = np.argsort(self.sorted_idxs, axis=0)
	def get_sorted(self, targets, feature):
		data_sorted_by_feature = self.x[self.sorted_idxs[:, feature]]
		mask = np.zeros((self.x.shape[0], ))
		mask[self.reverse_idxs[targets, feature]] = 1
		return (data_sorted_by_feature.compress(mask, axis=0), self.y[targets])

class TreeNode():
	def __init__(self, decision, description, children, surrogates=[]):
		self.decision = decision
		self.description = description
		self.surrogates = surrogates
		self.left, self.right = children
	def classify(self, x):
		if(self.decision(x)):
			return self.left.classify(x)
		else:
			return self.right.classify(x)
	def __str__(self):
		fmt = self.description
		fmt += ("\n\tLeft: " + "\n\t".join(str(self.left).split("\n")))
		fmt += ("\n\tRight: " + "\n\t".join(str(self.right).split("\n")))
		return fmt
	__repr__ = __str__

class TreeLeaf():
	def __init__(self, label):
		self.label = label
	def classify(self, x):
		return self.label
	def __str__(self):
		return "X is {0}".format(self.label)
	__repr__ = __str__

class Impurities():
	@staticmethod
	def _entropy(current_x, current_y, left_x, left_y, right_x, right_y):
		cur_freqs = Counter(current_y)
		total = len(current_y)
		cur_entropy =  -1 * sum([x / total * math.log2(x / total) for x in cur_freqs.values()])

		
		for new in [left_y, right_y]:
			new_freqs = Counter(new)
			new_total = len(new)
			new_entropy = -1 * sum([x / new_total * math.log2(x / new_total) for x in new_freqs.values()])
			cur_entropy -= (new_total / total) * new_entropy
		return cur_entropy
	@staticmethod
	def entropy():
		return Impurities._entropy

	@staticmethod
	def _variance(current_x, current_y, left_x, left_y, right_x, right_y):
		rv = 0
		lv = 0
		if(len(right_y) != 0):
			rv = np.var(right_y)
		if(len(left_y) != 0):
			lv = np.var(left_y)

		# The impurity is based on the variance in current_y. Ideally we have low variance.
		return np.var(current_y) - (len(left_y) / len(current_y) * lv) - (len(right_y) / len(current_y) * rv)
	@staticmethod
	def variance():
		return Impurities._variance

class StopCriteria():
	@staticmethod
	def min_impurity_drop(beta=1):
		return lambda splitter, impurity_drop, x_branches, y_branches: impurity_drop <= beta

# A split locator is a class that locates splits for a specific type of data
class SplitLocator():
	def __init__(self, impurity):
		self.impurity = impurity
	@staticmethod
	def split_on(x_vecs, y_vecs, decision_boundary):
		decisions = np.apply_along_axis(decision_boundary, 1, x_vecs)
		return ((x_vecs[decisions], y_vecs[decisions]), (x_vecs[~decisions], y_vecs[~decisions]))

	@staticmethod
	def arg_split_on(x_vecs, decision_boundary):
		decisions = np.apply_along_axis(decision_boundary, 1, x_vecs)
		return (decisions, ~decisions)

	def _eval_splits_on_feature(self, sorted_x, sorted_y, feature_idx):
		best_split = lambda x: True
		best_split_imp = (-math.inf)
		final_boundary = 0
		final_axis = 0
		for idx in range(1, len(sorted_x)):
			left_end = sorted_x[idx-1][feature_idx]
			right_end = sorted_x[idx][feature_idx]
			if(left_end == right_end):
				continue # don't bother with splits between the same value
			boundary = (1/2) * (left_end + right_end)

			impurity_drop = self.impurity(sorted_x, sorted_y, sorted_x[:idx], sorted_y[:idx], sorted_x[idx:], sorted_y[idx:])
			
			if(impurity_drop > best_split_imp):
				best_split_imp = impurity_drop
				final_boundary = boundary
				final_axis = feature_idx
		return (best_split_imp, final_boundary, final_axis)
	def _continuous_best_split(self, obs, labels):
		sort = np.argsort(obs, axis=0)

		with Pool(4) as pool:
			map_args = [(obs[sort[:,feature]], labels[sort[:,feature]], feature) for feature in range(obs.shape[1]) ]
			best_splits = pool.starmap(self._eval_splits_on_feature, map_args)

		best_split_imp, final_boundary, final_axis = max(best_splits, key=lambda v: v[0])
		best_split = lambda obs, b=final_boundary, f=final_axis: obs[f] > b
		description = "Is x{0} > {1} @ {2}?".format(final_axis, final_boundary, best_split_imp)

		return (best_split, best_split_imp, description)
	def continuous_best_split(self):
		return lambda x, y: self._continuous_best_split(x, y)

class DecisionTree():
	def __init__(self, stop_criteria, split_locator):
		self.stop_criteria = stop_criteria
		self.splitter = split_locator
	def fit(self, x, y):
		def _build_tree(x, y): 
			# idxs is a list of vectors representing x and y values at each node. Each index gives the location
			# of that x value when all x values are sorted by that column's feature.

			# State is a tuple (x_vecs, y_vecs, sorted_idxs)
			best_split, best_split_imp, description = self.splitter(x, y)
			if(self.stop_criteria(best_split, best_split_imp, x, y) or all(lbl == y[0] for lbl in y)):
				return TreeLeaf(np.average(y))
			else:
				(left_x, left_y), (right_x, right_y) = SplitLocator.split_on(x, y, best_split)
				return TreeNode(best_split, description, (_build_tree(left_x, left_y), _build_tree(right_x, right_y)))

		self.tree = _build_tree(x, y)
	def classify(self, x):
		try:
			return self.tree.classify(x)
		except AttributeError:
			raise Exception("Cannot classify an example without first fitting a tree")
	def __str__(self):
		try:
			return str(self.tree)
		except AttributeError:
			return "Empty decision tree"
	def mean_error(self, x, y):
		# average distance from predictions to labels
		predicted = np.apply_along_axis(self.classify, 1, x)
		return np.mean(np.abs(predicted - y))
	def mean_square_error(self, x, y):
		predicted = np.apply_along_axis(self.classify, 1, x)
		return np.mean(np.square(predicted - y))
	def classification_error(self, x, y):	
		predicted = np.apply_along_axis(self.classify, 1, x)
		return (np.sum(np.abs(predicted - y)) / len(y))
	def accuracy(self, x, y):
		predicted = np.apply_along_axis(self.classify, 1, x)
		thresholds = predicted[predicted >= 0.5].astype(int)
		return 1 - np.sum(np.abs(predicted - y)) / len(y)

if __name__ == "__main__":
	def k_folds(k, x_data, y_data):
		return [(x_data[i::k], y_data[i::k]) for i in range(k)]
		
	print("Using Boston Housing Data" + "\n" + "="*50)
	train_data = np.genfromtxt("../data/housing_train.txt")
	train_x = train_data[:, :-1]
	train_y = train_data[:, -1]
	dt = DecisionTree(StopCriteria.min_impurity_drop(15), SplitLocator(Impurities.variance()).continuous_best_split())
	dt.fit(train_x, train_y)

	test_data = np.genfromtxt("../data/housing_test.txt")
	test_x = test_data[:, :-1]
	test_y = test_data[:, -1]
	print("Mean squared training error: " + str(dt.mean_square_error(train_x, train_y)))
	print("Mean squared testing error: " + str(dt.mean_square_error(test_x, test_y)))
	print("\n\n\n\n")

	
	print("Using Spambase Data" + "\n" + "="*50)
	train_data = np.genfromtxt("../data/spambase.data", delimiter=",")
	train_x = train_data[:, :-1]
	train_y = train_data[:, -1]
	dt = DecisionTree(StopCriteria.min_impurity_drop(0.01), SplitLocator(Impurities.entropy()).continuous_best_split())


	k = 5
	print("Calculating k-fold cross validation, k={0}".format(k))
	folds = k_folds(k, train_x, train_y)
	net_test_error = 0

	for idx in range(k):
		print("\rFitting {0}...".format(idx), end="")
		test_x, test_y = folds[idx]
		train_sets = [folds[x] for x in range(k) if x != idx]
		train_x = np.concatenate([item[0] for item in train_sets])
		train_y = np.concatenate([item[1] for item in train_sets])

		dt.fit(train_x, train_y)
		net_test_error += dt.accuracy(test_x, test_y)
	avg_test_error = net_test_error / k
	print("\rAverage testing accuracy {0} folds: {1}".format(k, avg_test_error))
	






