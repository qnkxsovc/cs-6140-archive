import scipy.stats
import numpy as np
import pdb
# Estimation Maximization for fitting a gaussian mixture model to data

class EM():
	def __init__(self, n_gaussians):
		self.n_gaussians = n_gaussians
		self.parameters = -1
	def fit(self, all_x, n_iter=10, epsilon=0.01, verbose=False):
		param = {}
		for i in range(self.n_gaussians):
			fold = all_x[i * len(all_x) // self.n_gaussians : (i+1) * len(all_x) // self.n_gaussians]
			param[i] = (1/self.n_gaussians, np.mean(fold, axis=0), np.cov(fold, rowvar=False) + (np.identity(all_x.shape[1]) * epsilon))
		# each gaussian has a parameter vector of elements (mx, mn, cov)
		mix = np.zeros((len(all_x), self.n_gaussians)) 
		# initialize the mixture coefficients to be uniform

		def p_k(x, k, with_mix=False):
			# return the current likelihood of x given generating distribution k
			mx, mn, cov = param[k]
			if(not with_mix):
				return scipy.stats.multivariate_normal.pdf(x, mean=mn, cov=cov)
			else:
				return scipy.stats.multivariate_normal.pdf(x, mean=mn, cov=cov) * mx
		def e_step():
			# update the mixture coefficients in place
			for i in range(len(all_x)):
				unnormalized = np.array([p_k(all_x[i], k, with_mix=True) for k in range(self.n_gaussians)])
				mix[i] = (unnormalized / np.sum(unnormalized))
		def m_step():
			# update param by resetting each of the parameter vectors
			# start with mean
			norms = np.sum(mix, axis=0)
			mn_matrix = (mix.T @ all_x) / norms[:, None]
			for k in range(self.n_gaussians):
				shift = (all_x - mn_matrix[k, :])
				cov = (shift.T @ (shift * mix[:, k, None])) / norms[k] + (np.identity(all_x.shape[1]) * epsilon)
				param[k] = (norms[k]/len(all_x), mn_matrix[k, :], cov)
		for i in range(n_iter):
			if(verbose):
				print(f"\rIteration {i}", end="")
			e_step()
			m_step()

		if(verbose):
			print("\r========== Parameters ==========")
			for k in range(self.n_gaussians):
				print(f"\tGaussian {k}")
				mx, mn, cov = param[k]
				print("\n\t - ".join([
					f"weight: {mx}",
					f"mean: {mn}",
					f"covariance: {cov}"
					]))

		self.parameters = param
	def event_likelihood(self, x):
		return sum([ mx * scipy.stats.multivariate_normal(x, mn, cov) for mx, mn, cov in self.parameters.items()])
	def label_likelihood(self, x, y, log=False):
		self._assert_fit()
		mx, mn, cov = self.parameters[y]
		return scipy.stats.multivariate_normal(x, mn, cov)
	def classify(self, x):
		return max(range(self.n_gaussians), key = lambda y_i: self.label_likelihood(x, y_i))
	def batch_accuracy(self, x, y):
		num_correct = 0
		for x_ins, y_ins in zip(x, y):
			num_correct += int((self.classify(x_ins) == y_ins).all())
		return num_correct / len(x)
			
	def _assert_fit(self):
		if(self.parameters == -1):
			raise Error("Cannot make a prediction before the model has been fit.")

