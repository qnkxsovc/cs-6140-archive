import numpy as np
import math
import pdb
from collections import defaultdict

class Atom():
	'''
	Base class for representing single operations in a differentiable model.
	'''
	def __init__(self):
		'''
		Initializes internal variables required for taking gradients
		'''
		self.grad_dependencies = {}
		self.cached_grads = defaultdict(int) # defaults to 0
		self.cached_symbolic_grads = defaultdict(lambda: Const(0)) # defaults to 0

	def eval(self, parameters):
		'''
		Given a state dictionary, evaluate this operation.

		Parameters:
		----------
		parameters: a dictionary {String: Variable, ...} containing the values for all parameters required to evaluate this operation
		'''
		return self.numeric_eval(*[child.eval(parameters) for child in self.children])

	def numeric_eval(self, *args):
		'''
		By default, return the first argument. If this function is not overridden it is identity()

		Parameters:
		----------
		*args: Arbitrary number of inputs
		'''
		return args[0]

	def _forward_prop(self, parameters):
		'''
		Helper method that performs forward propagation. The output of each operation is stored in its cached_eval attribute.

		Parameters:
		----------
		parameters: a dictionary {String: Variable, ...} containing the values for all parameters required to evaluate this operation
		'''
		try:
			return self.cached_eval # If this node has already been evaluated, use that result
		except AttributeError:
			context = {}
			for child in self.children:
				context = {**child._forward_prop(parameters), **context}
			context[self.name] = self.numeric_eval(*[context[child.name] for child in self.children])
			self.cached_eval = context
			return self.cached_eval

	def forward_prop(self, parameters, cleanup=True):
		'''
		Calculate the ouptuts of all operations in the model. Returns a dictionary of each operation and its output. 

		Parameters:
		----------
		parameters: a dictionary {String: Variable, ...} containing the values for all parameters required to evaluate this operation
		cleanup: if True, this method will remove cached forward propagations once finished.
		'''
		self._forward_prop(parameters)
		result = self.cached_eval
		if(cleanup):
			self.clear_cached_eval()
		return result

	def _backprop(self, parameters, parent_grad_op):
		'''
		Helper method that recursively performs backpropagation.
		Each child operation stores the rate of change of the root output with respect to the child operation's inputs.
		The output node is determined by parent calls to this method – see backprop().

		Parameters:
		----------
		parameters: a dictionary {String: Variable, ...} containing the values for all parameters required to evaluate this operation
		'''
		for child in self.children:
			child_grad_op = self.grad_dependencies[child](parent_grad_op)
			out_wrt_child = child_grad_op._forward_prop(parameters)[child_grad_op.name]
			self.cached_grads[child] += out_wrt_child
			self.cached_symbolic_grads[child] = Sum(self.cached_symbolic_grads[child], child_grad_op)
			child._backprop(parameters, child_grad_op)

	def _symbolic_backprop(self, parent_grad_op):
		'''
		Helper method to build a model of the gradient without actually evaluating it for a specific instance.
		
		Parameters:
		----------
		parent_grad_op: an operation that represents the gradient of model output with respect to this atom's output
		'''
		for child in self.children:
			child_grad_op = self.grad_dependencies[child](parent_grad_op)
			self.cached_symbolic_grads[child] = Sum(self.cached_symbolic_grads[child], child_grad_op)
			child._symbolic_backprop(child_grad_op)

	def backprop(self, parameters, cleanup=True):
		'''
		Calculate the gradient of this operation's output with respect to all child operations' inputs. If cleanup is True, node forward
		propagations will be removed once the gradients are calculated. Note that backpropagations will not be removed in either case.

		Parameters:
		----------
		parameters: a dictionary {String: Variable, ...} containing the values for all parameters required to evaluate this operation
		cleanup: if True, this method will remove cached forward propagations once finished.
		'''
		self._forward_prop(parameters)
		self._backprop(parameters, Const(np.ones(self.shape), name="1"))
		if(cleanup):
			self.clear_cached_eval()

	def symbolic_backprop(self):
		'''
		Recursively build the model representing the gradient of each node with respect to function output
		'''
		self._symbolic_backprop(Const(np.ones(self.shape), name="1"))

	def grad_wrt(self, item, parameters, cleanup=True):
		'''
		Caclulate the gradient of this node with respect to a child. 
		If cleanup is True (default), removes cached forward and back propagations once finished.
		
		Parameters:
		----------
		item: The child
		parameters: a dictionary {String: Variable, ...} containing the values for all parameters required to evaluate this operation
		cleanup: if True, this method will remove cached forward and back propagations once finished.
		'''
		self.backprop(parameters, cleanup=True)
		grad = item.cached_grads[item]
		if cleanup:
			self.clear_cached_grads()
			self.clear_cached_symbolic_grads()
		return grad

	def symbolic_grad_wrt(self, item, cleanup=True):
		'''
		Caclulate the symbolic gradient of this node with respect to a child. 
		If cleanup is True (default), removes cached forward and back propagations once finished.
		
		Parameters:
		----------
		item: The child
		parameters: a dictionary {String: Variable, ...} containing the values for all parameters required to evaluate this operation
		cleanup: if True, this method will remove cached forward and back propagations once finished.
		'''
		self.symbolic_backprop()
		grad = item.cached_symbolic_grads[item]
		if cleanup:
			self.clear_cached_grads()
			self.clear_cached_symbolic_grads()
		return grad

	def clear_cached_eval(self):
		'''
		Clear the cached evaluations of this node and all children.
		'''
		self.__dict__.pop("cached_eval", None)
		[child.clear_cached_eval() for child in self.children]

	def clear_cached_grads(self):
		'''
		Clear the cached gradients of this node and all children.
		'''
		self.cached_grads = defaultdict(int)
		[child.clear_cached_grads() for child in self.children]
	
	def clear_cached_symbolic_grads(self):
		'''
		Clear the cached symbolic gradients of this node and all children.
		'''
		self.cached_symbolic_grads = defaultdict(lambda: Const(0))
		[child.clear_cached_symbolic_grads() for child in self.children]

	def _get_parameters(self):
		'''
		Helper to return all parameters that are children of this model
		'''
		d = {}
		for child in self.children:
			d.update(child._get_parameters())
		return d
	def get_parameters(self):
		'''
		Return all parameters that are children of this model
		'''
		return list(self._get_parameters().values())
	
class BinAtom(Atom):
	def __init__(self, param_one, param_two):
		super().__init__()
		self.children = [param_one, param_two]
		self.left = param_one
		self.right = param_two

class UnAtom(Atom):
	def __init__(self, param_one):
		super().__init__()
		self.children = [param_one]
		self.center = param_one

class Variable():
	def __init__(self, name, shape):
		self.name = name
		self.shape = shape
		self.children = []
		self.cached_grads = defaultdict(int)
		self.cached_symbolic_grads = defaultdict(lambda: Const(0))

	def clear_cached_eval(self):
		self.__dict__.pop("cached_eval", None)
	def clear_cached_grads(self):
		self.cached_grads = defaultdict(int)
	def clear_cached_symbolic_grads(self):
		self.cached_symbolic_grads = defaultdict(lambda: Const(0))

	def eval(self, parameters):
		raise NotImplemented("Can't call eval on the Variable base class")

	def _forward_prop(self, inputs):
		self.cached_eval = {self.name: self.eval(inputs)}
		return self.cached_eval
	def _backprop(self, parameters, parent_grad_op):
		self.cached_grads[self] += parent_grad_op._forward_prop(parameters)[parent_grad_op.name]
		self.cached_symbolic_grads[self] = Sum(self.cached_symbolic_grads[self], parent_grad_op)
	def _symbolic_backprop(self, parent_grad_op):
		self.cached_symbolic_grads[self] = Sum(self.cached_symbolic_grads[self], parent_grad_op)

	def get_parameters(self):
		return self._get_parameters(self)
class Scale(BinAtom):
	def __init__(self, param_one, param_two):
		super().__init__(param_one, param_two)
		assert (param_one.shape == (1,) or param_two.shape == (1,) or param_one.shape == param_two.shape)
		self.shape = param_two.shape
		self.name = "{0}*{1}".format(*[child.name for child in self.children])
		self.grad_dependencies = {
			self.left: lambda parent_grad_op: Scale(parent_grad_op, self.right),
			self.right: lambda parent_grad_op: Scale(parent_grad_op, self.left)
		}
	def numeric_eval(self, x, y):
		return x * y

class Dot(BinAtom):
	def __init__(self, param_one, param_two):
		super().__init__(param_one, param_two)
		assert param_one.shape == param_two.shape
		self.shape = (1,)
		self.name = "{0}·{1}".format(*[child.name for child in self.children])
		self.grad_dependencies = {
			self.left: lambda parent_grad_op: Scale(parent_grad_op, self.right),
			self.right: lambda parent_grad_op: Scale(parent_grad_op, self.left)
		}
	def numeric_eval(self, x, y):
		return np.dot(x, y)

class MatMul(BinAtom):
	def __init__(self, param_one, param_two):
		super().__init__(param_one, param_two)
		assert param_one.shape[1] == param_two.shape[0]
		self.shape = (param_one.shape[0], param_two.shape[1])
		self.name = "{0} @ {1}".format(*[child.name for child in self.children])

		self.grad_dependencies = {
			self.left: lambda parent_grad_op: MatMul(parent_grad_op, Transpose(self.right)),
			self.right: lambda parent_grad_op: MatMul(Transpose(self.left), parent_grad_op)
		}
	def numeric_eval(self, x, y):
		return np.matmul(x, y)

class Difference(BinAtom):
	def __init__(self, param_one, param_two):
		super().__init__(param_one, param_two)
		self.name = "({0} - {1})".format(*[child.name for child in self.children])
		if(param_one.shape == (1,)):
			self.shape = param_two.shape
		elif (param_two.shape == (1,)):
			self.shape = param_one.shape
		else:
			assert param_one.shape == param_two.shape
			self.shape = param_one.shape
		self.grad_dependencies = {
			self.left: lambda parent_grad_op: parent_grad_op,
			self.right: lambda parent_grad_op: Negate(parent_grad_op)
		}
	def numeric_eval(self, x, y):
		return x - y

class Sum(BinAtom):
	def __init__(self, param_one, param_two):
		super().__init__(param_one, param_two)
		self.name = "({0} + {1})".format(*[child.name for child in self.children])
		if(param_one.shape == (1,)):
			self.shape = param_two.shape
		elif (param_two.shape == (1,)):
			self.shape = param_one.shape
		else:
			assert param_one.shape == param_two.shape
			self.shape = param_one.shape
		self.grad_dependencies = {
			self.left: lambda parent_grad_op: parent_grad_op,
			self.right: lambda parent_grad_op: parent_grad_op
		}
	def numeric_eval(self, x, y):
		return x + y

class Frac(BinAtom):
	def __init__(self, param_one, param_two):
		super().__init__(param_one, param_two)
		self.name = "({0} / {1})".format(*[child.name for child in self.children])
		if(param_one.shape == (1,)):
			self.shape = param_two.shape
		elif (param_two.shape == (1,)):
			self.shape = param_one.shape
		else:
			assert param_one.shape == param_two.shape
			self.shape = param_one.shape
		self.grad_dependencies = {
			self.left: lambda parent_grad_op: Frac(parent_grad_op, self.right),
			self.right: lambda parent_grad_op: Scale(parent_grad_op, Frac(Negate(self.left), Sqr(self.right)))
		}
	def numeric_eval(self, x, y):
		return x / y

class Pwr(UnAtom):
	def __init__(self, param_one, expt):
		super().__init__(param_one)
		self.name = "{0}^{1}".format(self.center.name, expt)
		self.shape = param_one.shape
		self.expt = expt
		self.base = self.center
		self.grad_dependencies = {
			self.base: lambda parent_grad_op: Scale(parent_grad_op, Scale(Const(self.expt), Pwr(self.base, self.expt - 1)))
		}
	def numeric_eval(self, x):
		return x ** self.expt

class Sqr(UnAtom):
	def __init__(self, param_one):
		super().__init__(Pwr(param_one, 2))
		self.name = "Sqr({0})".format(param_one.name)
		self.shape = param_one.shape
		self.grad_dependencies = {
			self.center: lambda parent_grad_op: parent_grad_op
		}

class Exp(UnAtom):
	def __init__(self, param_one, base=math.e):
		super().__init__(param_one)
		self.name = "{0}^({1})".format(str(round(base, 2)), *[child.name for child in self.children])
		self.shape = param_one.shape
		self.expt = self.center # naming for convenience
		self.base = base
		self.grad_dependencies = {
			self.expt: lambda parent_grad_op: Scale(Scale(Const(math.log(base)), self), parent_grad_op)
		}
	def numeric_eval(self, x):
		return self.base ** x

class Const(Atom):
	def __init__(self, value, name=""):
		super().__init__()
		self.val = value
		if name == "":
			self.name = str(value)
		else:
			self.name = name
		if(isinstance(value, (int, complex, float))):
			self.shape = (1,)
		elif(isinstance(value, np.ndarray)):
			self.shape = value.shape
		else:
			raise Exception("This value cannot be used as a constant")
		self.children = []
	def eval(self, parameters):
		return self.val
	def numeric_eval(self):
		return self.val

class Transpose(UnAtom):
	def __init__(self, param_one):
		super().__init__(param_one)
		self.name = "{0}^T".format(*[child.name for child in self.children])
		self.shape = (param_one.shape[1], param_one.shape[0])
		self.grad_dependencies = {
			self.center: lambda parent_grad_op: Transpose(parent_grad_op)
		}
	def numeric_eval(self, x):
		return x.T

class MSE(BinAtom):
	def __init__(self, param_one, param_two):
		super().__init__(param_one, param_two)
		self.name = "MSE({0}, {1})".format(*[child.name for child in self.children])
		self.shape = (1,)
		self.predictor = self.left
		self.label = self.right
		self.grad_dependencies = {
			self.predictor: lambda parent_grad_op: Scale( Difference(self.predictor, self.label), parent_grad_op),
			self.label: lambda parent_grad_op: Negate(Scale(Difference(self.predictor, self.label), parent_grad_op))
		}
	def numeric_eval(self, x, y):
		return (1/2) * (np.linalg.norm(x - y) ** 2)

class Sigmoid(UnAtom):
	def __init__(self, param_one):
		super().__init__(Frac(Exp(param_one), Sum(Const(1), Exp(param_one))))
		self.name = "σ({0})".format(param_one.name)
		self.shape = param_one.shape
		self.grad_dependencies = {
			self.center: lambda parent_grad_op: parent_grad_op
		}

class Negate(UnAtom):
	def __init__(self, param_one):
		super().__init__(Scale(Const(-1), param_one))
		self.name = "(-{0})".format(param_one.name)
		self.shape = param_one.shape
		self.grad_dependencies = {
			self.center: lambda parent_grad_op: parent_grad_op
		}
		
class SumComponents(UnAtom):
	def __init__(self, param_one):
		super().__init__(param_one)
		self.name = "SumComponents({0})".format(*[child.name for child in self.children])
		self.shape = (1,)
		self.grad_dependencies = {
			self.center: lambda parent_grad_op: Scale(parent_grad_op, Const(np.ones(self.center.shape)))
		}
	def numeric_eval(self, x):
		return np.sum(x)

class Placeholder(Variable):
	def __init__(self, name, shape):
		super().__init__(name, shape)
	def eval(self, inputs):
		try:
			return inputs[self.name]
		except KeyError:
			raise Exception("The model is dependent on an input that was not provided")
	def _get_parameters(self):
		return {}

class Parameter(Variable):
	def __init__(self, name, init):
		super().__init__(name, init.shape)
		self.init = init
		self.val = np.copy(init)
	def eval(self, inputs):
		return self.val
	def __add__(self, other):
		if(not isinstance(other, (np.ndarray, float, int))):
			raise TypeError("Cannot add {0} to parameter".format(type(other)))
		self.val += other
		return self
	def __sub__(self, other):
		if(not isinstance(other, (np.ndarray, float, int))):
			raise TypeError("Cannot subtract {0} from parameter".format(type(other)))
		self.val -= other
		return self
	def reset(self):
		self.val = np.copy(self.init)

	def _get_parameters(self):
		return {self.name : self}


class Model():
	def __init__(self, output, loss, instance_template=("x", "y")):
		self.output = output
		self.loss = loss
		self.instance_template = instance_template
	def train_step(self, instance, learn_rate = 0.01):
		params = self.output.get_parameters()
		grads = [self.loss.grad_wrt(param, instance) for param in params]
		for idx in range(len(params)):
			params[idx] -= learn_rate * grads[idx]
	def train_on(self, x, y, learn_rate=0.01, n_iter = 5000):
		for idx in range(n_iter):
			idx = idx % len(x)
			ins = {self.instance_template[0]: x[idx], self.instance_template[1]: y[idx]}
			self.train_step(ins, learn_rate=learn_rate)
	def mean_error(self, x, y):
		error = 0
		for obs, label in zip(x, y):
			ins = {self.instance_template[0]: obs, self.instance_template[1]: label}
			error += abs(self.loss.eval(ins))
		return error / len(x)
