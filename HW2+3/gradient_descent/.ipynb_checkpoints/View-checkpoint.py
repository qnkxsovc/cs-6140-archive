import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

class BinaryEvaluator():
	def __init__(self, controller):
		self.model = controller
	def confusion_matrix(self, eval_x, eval_y, threshold=0.5):
		mat = np.zeros((2, 2))
		results = (self.model.eval(eval_x) > threshold).astype(int)
		for out, tar in zip(results, eval_y):
            print((out != tar).astype(int))
			mat[(out != tar).astype(int)[0]][(1 - out).astype(int)[0]] += 1
		return pd.DataFrame(mat, index=["true", "false"], columns=["pos", "neg"])
	def true_pos_rate(self, eval_x, eval_y, threshold=0.5):
		cm = self.confusion_matrix(eval_x, eval_y, threshold).to_numpy()
		return cm[0][0] / (cm[0][0] + cm[1][1])
	def false_pos_rate(self, eval_x, eval_y, threshold=0.5):
		cm = self.confusion_matrix(eval_x, eval_y, threshold).to_numpy()
		return cm[1][0] / (cm[1][0] + cm[0][1])
	def _points_on_roc(self, eval_x, eval_y):
		thresholds = [0] + self.model.eval(eval_x).flatten().tolist()
		return [(self.false_pos_rate(eval_x, eval_y, threshold=t), self.true_pos_rate(eval_x, eval_y, threshold = t)) for t in thresholds]
	def show_roc(self, eval_x, eval_y, threshold_incr=10):
		x, y = zip(*self._points_on_roc(eval_x, eval_y))
		plt.plot(x, y, "xb")
		plt.xlim((0, 1))
		plt.ylim((0, 1))
		plt.show()
	def auc(self, eval_x, eval_y, threshold_incr=10):
		x, y = zip(*self._points_on_roc(eval_x, eval_y))
		return sum( [(x[i] - x[i+1] ) * y[i] for i in range(len(x) - 1)] ) # x[-1] is always 0