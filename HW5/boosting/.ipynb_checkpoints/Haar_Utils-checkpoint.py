from collections import defaultdict
import numpy as np

def to_haar_features(map, features):
	'''
	Given the Haar map of an image and a list of haar features, make a feature vector by evaluating
	each feature against the map
	:param map: the Haar Map of an image
	:param features: a list of Haar Features
	:return: np.ndarray
	'''
	return np.array([eval_haar_feature(map, f) for f in features])

def make_haar_map(img):
	'''
	Make a Haar map given an image.
	:param img: a 2D greyscale image.
	:return: np.ndarray
	'''
	m, r = np.min(img), np.ptp(img)
	img = (img - m) / r
	b = defaultdict(int)

	for i in range(img.shape[0]):
		for j in range(img.shape[1]):
			pix_black = img[i, j]
			b[(i, j)] = b[(i-1, j)] + b[(i, j-1)] - b[(i-1, j-1)] + pix_black
	return dict(b)



def eval_haar_feature(mp, feature):
	'''
	Evaluate an individual Haar feature against a Haar map
	:param mp: a Haar map
	:param feature: a Haar feature
	:return: scalar
	'''

	def haar_map_lookup(mp, points):
		'''
		a . . . b
		. . . . .
		. . . . .
		c . . . d
		'''
		a, b, c, d = points
		return mp[d] - mp[c] - mp[b] + mp[a]

	h_or_v, a, b, c, d, e, f = feature
	if(h_or_v == 1):
		# horizontal
		return haar_map_lookup(mp, (a, b, c, d)) - haar_map_lookup(mp, (c, d, e, f))
	else:
		return haar_map_lookup(mp, (a, c, b, d)) - haar_map_lookup(mp, (c, e, d, f))

def find_haar_feature(im_shape):
	'''
	Find a random Haar feature
	:return: Haar Feature
	'''
	# horizontal feature
	tl = (np.random.randint(0, im_shape[0] - 5), np.random.randint(0, im_shape[0] - 5))
	h, w = np.random.randint(5, im_shape[1] - tl[1]), np.random.randint(5, im_shape[0] - tl[0])
	br = (tl[0] + w, tl[1] + h)

	'''
	a . . . . . b
	. . . . . . .
	c . . . . . d
	. . . . . . .
	e . . . . . f
	'''
	a, b = tl, (br[0], tl[1])
	e, f = (tl[0], br[1]), br
	c, d = (a[0], int((a[1] + e[1])/2)), (b[0], int((b[1] + f[1])/2))
	horiz_feature = (1, a, b, c, d, e, f)

	'''
	a . c . e
	. . . . .
	. . . . .
	. . . . .
	. . . . .
	. . . . .
	b . d . f
	'''
	a_, b_ = tl, (tl[0], br[1])
	e_, f_ = (br[0], tl[1]), br
	c_, d_ = (int((a_[0] + e_[0])/2), a_[1]), (int((b_[0] + f_[0])/2), b_[1])
	vert_feature = (0, a_, b_, c_, d_, e_, f_)
	return (horiz_feature, vert_feature)
