import numpy as np
import scipy.stats
import copy 

# Base class for implementing generative models
# In order to model the likelihood of different labels given an observation, generative models predict the likelihood of
#     the label given the observation and then use Bayes' rule to reverse the result
# This class provides the mechanics for estimating the parameters of Bayes' theorem, as each generative model will accomplish
#     this in the same way

class Generative():
	def observation_likelihood(self, obs):
		'''
		This is a stub that subclasses will override on their own.

		Parameters:
		----------
		obs: a single data observation
		'''
		raise NotImplementedException("A generative model will override this method on its own")

	def label_likelihood(self, obs, label):
		'''
		Given an observation and a label, return the likelihood of the label given the observation
		
		Parameters:
		----------
		obs: a single data observation
		label: a given data label
		'''
		try:
			general_p_x = sum([self.observation_likelihood(obs, l) * self.general_class_likelihoods[l] for l in self.labels ])
		except AttributeError:
			raise Exception("The model must be fit before it can provide a label likelihood")

		return self.observation_likelihood(obs, label) * self.general_class_likelihoods[label] / general_p_x

	def _calculate_class_likelihoods(self, labels):
		'''
		Given the label vector for a set of observations, calculate the naive likelihood of those labels. 

		Parameters:
		----------
		labels: the label vector for a set of observations
		'''
		num_observations = len(labels)
		labels, counts = np.unique(labels, return_counts=True)
		self.labels = labels
		self.general_class_likelihoods = {labels[i]: counts[i] / num_observations for i in range(len(labels))}
		
	def test(self, x, y):
		'''
		Test the model on a set of observations x with labels y. This function will return the model's average certainty
		that the actual label is correct.
	
		Parameters:
		----------
		x: an MxN matrix of M observations each with N features
		y: an M dimensional vector of class labels
		'''
		return np.mean([self.label_likelihood(obs, label) for obs, label in zip(x, y)])
	
	def error(self, x, y):
		'''
		Returns 1 - (average model certainty in correct value)
		
		Parameters:
		----------
		x: an MxN matrix of M observations each with N features
		y: an M dimensional vector of class labels
		'''
		return 1 - self.test(x, y)
	
	def classify(self, obs):
		'''
		Given an observation, return the most likely label

		Parameters:
		----------
		obs: a single data observation
		'''
		try:
			return max(self.labels, lambda l: self.label_likelihood(obs, l))
		except AttributeError:
			raise Exception("self.labels not found. Has this model set the possible output labels?")
			

# Gaussian Descriminant Analysis
# Binary classification problem
# Algorithm
#     Fit a covariance matrix to the data
#     Separate the given data into positive and negative examples
#     Fit covariance matrices to the subsets of the data in each group
#     Calculate the mean of each subset
#     Calculate the P(Y) 
#     Calculate P(X)

class GDA(Generative):
	def __init__(self, shared_covariance=True):
		self.shared_covariance = shared_covariance
	def fit(self, x, y):
		'''
		Fit the gaussians for this model.
		
		Parameters:
		----------
		x: an MxN matrix of M observations each with N features
		y: an M dimensional vector of class labels
		'''
		
		data = self.bin_data(x, y)
		self.cov = {}
		self.means = {}
		self.labels = [subset[1] for subset in data]
		if(self.shared_covariance):
			observations = np.concatenate([subset[0] for subset in data], axis=0)

			cov = np.cov(observations, rowvar=False)
			self.cov = {label: cov for label in self.labels}
		for observations, label in data:
			if (not self.shared_covariance):
				self.cov[label] = np.cov(observations, rowvar=False)
			self.means[label] = np.mean(observations, axis=0)

		total_observations = sum([len(subset[0]) for subset in data])
		self.general_class_likelihoods = {label: observations.shape[0] / total_observations for observations, label in data}

		self.class_gaussians = {label: scipy.stats.multivariate_normal(mean=self.means[label], cov=self.cov[label]) for label in self.labels}
	def observation_likelihood(self, obs, label, log=False):
		'''
		Given an observation and a label, return the likelihood of the observation under the given label's model

		Parameters:
		----------
		obs: a single data observation
		label: a given data label
		'''
		
		try:
            if log:
                return self.class_gausians[label].logpdf(obs)
            else:
                return self.class_gaussians[label].pdf(obs)
		except AttributeError:
			raise Exception("The model must be fit to data before it can make predictions")
	
	@staticmethod
	def bin_data(x, y):
		'''
		This is a helper method to bin a set of observations and class labels into the format [(observations..., label), ...]
		
		Parameters:
		----------
		x: an MxN matrix of M observations each with N features
		y: an M dimensional vector of class labels
		'''
		return [(x[y == label], label) for label in np.unique(y)]


# Naive bayes estimation of the data
# Assume that features are independent and model p(x|y) as the joint p(x_1|y) * ... * p(x_n|y)
# User should be able to specify how they want to model the distributions as a parameter of the model class
# Ideal output would have some sort of dictionary as a class attribute that holds a pdf for each component

# How to model pdfs? - would be univariate, needs to give easy probability estimates. 


class NaiveBayes(Generative):
	def __init__(self, dist_generator):
		'''
		Takes an UPDGenerator or a list of UPDGenerators that will be used in creating the Naive Bayes model.
		If a list is provided, it must have one generator for each feature of the input data. Otherwise, the single model will be used
			for all features.

		Parameters:
		----------
		dist_generator: a UPDGenerator or a list of UPDGenerators. 
		'''
		self.distributions = dist_generator
	
	def fit(self, x, y):
		'''
		Given an MxN matrix of M observations x each with N features, and a vector of labels y, fit the Naive bayes model to the data.
		If this model was initialized with a list of UPDGenerators, N must be the length of that list.
		
		Parameters:
		----------
		x: an MxN matrix of M observations each with N features
		y: an M length vector of labels
		'''

		num_features = x.shape[1]
		if(type(self.distributions) == list):
			if(len(self.distributions) != num_features):
				raise ValueError("The given data has the wrong number of features")
		else:
			# Normalize the dist_generator representation
			self.distributions = [copy.deepcopy(self.distributions) for _ in range(num_features)]

		super()._calculate_class_likelihoods(y)
		_ = [self.distributions[i].populate(x[:,i], y) for i in range(num_features)]

	def observation_likelihood(self, obs, label):
		'''
		Given an observation and a label, return the joint likelihood of all the features under the Naive Bayes assumption.

		Parameters:
		----------
		obs: a single data observationi
		label: a given data label
		'''
		try: 
			return np.prod([self.distributions[feature_idx].likelihood(obs[feature_idx], label) for feature_idx in range(len(obs))])
		except AttributeError:
			raise Exception("The model must be fit to data before it can make predictions")

class UPDGenerator():
	# Univariate Probability Distribution
	def __init__(self):
		self.description = "Unpopulated UPD"
	def populate(self, x, y):
		'''
		Stub method for generating a UPD given x, a vector of observations, and y, the corresponding labels

		Parameters:
		----------
		x: a vector of observations representing a single feature
		y: a vector of labels for the x observations
		'''
		raise NotImplementedException("A subclass should implement this method")
	def likelihood(self, obs, label):
		'''
		Stub method for assigning the likelihood that a given observation has a given label.

		Parameters:
		----------
		x: a vector of observations representing a single feature
		y: a vector of labels for the x observations
		'''
		raise NotImplementedException("A subclass should implement this method")


class AboveMean(UPDGenerator):
	def __init__(self, smoothing=False):
		self.given = {}
		self.smoothing = smoothing
		self.description = "Unpopulated AboveMean Model"
	def populate(self, x, y):
		'''
		Generates a UPD that models the probability of a value x given class y as the observed likelihood of
		observing a value above the feature mean given y if x is above that mean, and below if x is below.

		Parameters:
		----------
		x: a vector of observations representing a single feature
		y: a vector of labels for the x observations
		smoothing: if True, laplacian smoothing will be applied to the distribution
		'''
		mean = np.mean(x)
		for label in np.unique(y):
			examples_with_label = x[y == label]

			count_all = len(examples_with_label)
			count_geq_mean = np.sum(examples_with_label >= mean)
			count_lt_mean = np.sum(examples_with_label < mean) 
			if(self.smoothing):
				count_lt_mean += 1
				count_geq_mean += 1
				count_all += 2
			prob_geq_than = count_geq_mean / count_all
			prob_less_than = count_lt_mean / count_all

			self.description = ("{0} if x >= {1} else {2}".format(prob_geq_than, mean, prob_less_than))
			self.given[label] = (mean, prob_geq_than, prob_less_than, lambda obs, m=mean, g=prob_geq_than, l=prob_less_than: g if obs >= m else l)

	def likelihood(self, obs, label):
		'''
		Return the probability of a value obs given a label as the observed likelihood of
		observing a value above the feature mean given y if x is above that mean, and below if x is below.

		Parameters:
		----------
		obs: observed value of a single feature
		label: specifies the label of the observation in the likelihood computation
		'''
		try:
			return self.given[label][3](obs) # !!!
		except AttributeError:
			raise Exception("The model cannot give likelihoods without first being populated with data.")




