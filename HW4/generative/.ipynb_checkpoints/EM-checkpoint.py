import scipy.stats
# Estimation Maximization for fitting a gaussian mixture model to data

class EM():
	def __init__(n_gaussians):
		self.n_gaussians = n_gaussians
		# (mean, cov, mixture_coeff)
		self.gaussian_params = [(-1, -1, -1) for _ in range(self.n_gaussians)]
	def fit(x, y):
		# Z tracks the ex
		z = np.full((x.shape[0], self.n_gaussians), 1/self.n_gaussians)

		def e_step(self):
            for idx in range(len(x)):
                mean = self.gaussian_params[idx][0]
                cov = self.gaussian_params[idx][1]
                p_x_given_z = scipy.stats.multivariate_normal(x, mean=mean, cov=cov)
                p_z = self.gaussian_params[idx][2] # the general likelihood of a specific distribution is the mixture coefficient