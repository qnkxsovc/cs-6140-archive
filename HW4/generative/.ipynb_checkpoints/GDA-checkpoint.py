import numpy as np
import scipy.stats
from Generative import Generative
# Gaussian Descriminant Analysis
# Binary classification problem
# Algorithm
#     Fit a covariance matrix to the data
#     Separate the given data into positive and negative examples
#     Fit covariance matrices to the subsets of the data in each group
#     Calculate the mean of each subset
#     Calculate the P(Y) 
#     Calculate P(X)

class GDA(Generative):
	def __init__(self, shared_covariance=True):
		self.shared_covariance = shared_covariance
	def fit(self, x, y):
		'''
		Fit the gaussians for this model.
		
        Parameters:
        ----------
        x: an MxN matrix of M observations each with N features
        y: an M dimensional vector of class labels
		'''
        
        data = self.bin_data(x, y)
		self.cov = {}
		self.means = {}
		self.labels = [subset[1] for subset in data]
		if shared_covariance:
			observations = np.concatenate(*[subset[0] for subset in data], axis=0)

			cov = np.cov(observations, rowvar=False)
			self.cov = {label: cov for label in self.labels}
		for observations, label in data:
			if not shared_covariance:
				cov = np.cov(observations, rowvar=False)
				self.cov[label] = cov
			mean = np.mean(observations, axis=0)
			self.means[label] = mean

		total_observations = sum([len(subset[0]) for subset in data])
		self.general_class_likelihoods = {label: len(observations) / total_observations for label, observations in data}

		self.class_gaussians = {scipy.stats.multivariate_normal(self.means[label], self.cov[label]) for label in self.labels}
	def observation_likelihood(self, obs, label):
		'''
		Given an observation and a label, return the likelihood of the observation under the given label's model

		Parameters:
		----------
		obs: a single data observation
		label: a given data label
		'''
		try:
			return self.class_gaussians[label].pdf(obs)
		except AttributeError:
			raise Exception("The model must be fit to data before it can make predictions")
            
    def test(self, x, y):
		'''
		Test the model on a set of observations x with labels y. This function will return the model's assessment of the
        likelihood of that occurrence. Observations are assumed to be independent.
    
        Parameters:
        ----------
        x: an MxN matrix of M observations each with N features
        y: an M dimensional vector of class labels
		'''
        return np.prod(self.class_likelihood(obs, label) for obs, label in zip(x, y))
    @staticmethod
    def bin_data(x, y):
        '''
        This is a helper method to bin a set of observations and class labels into the format [(observations..., label), ...]
        
        Parameters:
        ----------
        x: an MxN matrix of M observations each with N features
        y: an M dimensional vector of class labels
        '''
        idxs_per_label = [(label, y.argwhere(y == label)) for label in np.unique(y)]
        return [(x[idxs], label) for label, idxs in idxs_per_label]