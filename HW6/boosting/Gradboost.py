import numpy as np
import math

class Gradboost():
	def __init__(self, stumps="best"):
		self.stumps = -1
	def fit(self, all_x, all_y, num_stumps=100):
		# Fit a depth-1 regression tree to the data by finding the best stump
		# Then, subtract the predicted label from the actual label for each point

		def find_best_inexact(y_vals, bins=10):
			best_splits = []

			for k in range(all_x.shape[1]):
				best_split, best_var_dif = None, math.inf
				best_vals = -1

				features = all_x[:, k]
				thresh_candidates = np.linspace(np.min(features), np.max(features), num=bins)
				for c in thresh_candidates[1:]:
					classified_pos_idxs = features > c
					pos = y_vals[classified_pos_idxs]
					neg = y_vals[~classified_pos_idxs]
					if(len(pos) > 0 and len(neg) > 0):
						weighted_var = len(pos)/len(features) * np.var(pos) + \
										len(neg)/len(features) * np.var(neg)
						if(weighted_var < best_var_dif):
							u = np.mean(pos)
							l = np.mean(neg)
							best_split = lambda x, k_=k, c_=c, u_=u, l_=l: u_ if x[k_] > c_ else l_
							best_var_dif = weighted_var
							best_vals = (c, k)
				best_splits.append((best_split, best_var_dif, best_vals))
			return min(best_splits, key=lambda i: i[1])


		acc_y = all_y.copy()
		stumps = []
		for i in range(num_stumps):
			best_stump, best_var, v = find_best_inexact(acc_y)
			for idx in range(len(all_x)):
				acc_y[idx] -= best_stump(all_x[idx])
			stumps.append(best_stump)
		self.stumps = stumps

	def score(self, x):
		score = np.sum( [self.stumps[i](x) for i in range(len(self.stumps))])
		return score
	def mse(self, x, y):
		sse = 0
		for x_ins, y_ins in zip(x, y):
			sse += (y_ins - self.score(x_ins))**2
		return sse / len(x)
	def _assert_fit(self):
		if(self.alphas == -1 and self.stumps == -1):
			raise Exception("Model cannot be used before it is fit to data")


