import tensorflow as tf
import numpy as np
import pdb
import os


def unique_vectors(inp_array):
	return len(np.unique( (inp_array > 0.5).astype(int), axis=0))

x = tf.placeholder(tf.float32, shape=[None, 8])
y = tf.placeholder(tf.float32, shape=[None, 8])

in_hid = tf.Variable(tf.random_uniform([8, 3]))
out_hid = tf.Variable(tf.random_uniform([3, 8]))


hidden = tf.math.sigmoid(tf.linalg.matmul(x, in_hid))
out = tf.math.sigmoid(tf.linalg.matmul(hidden, out_hid))
#out_activ = tf.linalg.matmul(hidden, out_hid)


loss = tf.losses.mean_squared_error(y, out)
#loss = tf.losses.softmax_cross_entropy(y, out_activ)
optimizer = tf.train.GradientDescentOptimizer(0.01)
train_op = optimizer.minimize(loss)



# find the run number
finished = False
rn = 0
dirs = os.listdir("ae_logs/")
while(not finished):
	if not str(rn) in dirs:
		finished = True
	else:
		rn += 1

tf.summary.image("in_weights", tf.reshape(tf.transpose(in_hid), (1, 3, 8, 1)))
tf.summary.image("out_weights", tf.reshape(out_hid, (1, 3, 8, 1)))
tf.summary.image("grads", tf.reshape(tf.transpose(tf.gradients(loss, in_hid)), (1, 3, 8, 1)))
tf.summary.histogram("grads_histogram", tf.gradients(loss, in_hid))
tf.summary.scalar("loss", loss)
tf.summary.scalar("num_vecs", tf.py_func(unique_vectors, [in_hid], tf.int64))

merged = tf.summary.merge_all()
writer = tf.summary.FileWriter("ae_logs/" + str(rn) + "/")
# ==================================



vecs = np.identity(8)

init = tf.global_variables_initializer()
sess = tf.InteractiveSession()

sess.run(init)
for step in range(100000):
	logs, _ = sess.run([merged, train_op], feed_dict={x: vecs[step % 8].reshape((1, 8)), y: vecs[step % 8].reshape((1, 8))})
	writer.add_summary(logs, step)

w = sess.run([hidden], feed_dict={x: vecs})[0]
w = (w > 0.5).astype(int)

merged = tf.summary.merge_all()

print(w)
print(len(np.unique(w, axis=0)))
pdb.set_trace()
