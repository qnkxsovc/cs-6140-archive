import numpy as np


class Polynomial():
	def __init__(self, p=2, c=-1):
		self.p = p
		self.c = c
	def kernel(self, known_data, test_ins):
		k = known_data @ test_ins.reshape((-1, 1))
		return (k.flatten() + self.c) ** self.p

class Euclidean():
	def __init__(self):
		pass
	def kernel(self, known_data, test_ins):
		scores = np.sqrt( np.sum( (known_data - test_ins.reshape((1, -1)))**2, axis=1) )
		return -1 * scores

class Cosine():
	def __init__(self):
		pass
	def kernel(self, known_data, test_ins):
		known_data = known_data / np.linalg.norm(known_data, axis=1).reshape((-1, 1))
		return (known_data @ (test_ins / np.linalg.norm(test_ins))).reshape((-1, 1)).flatten()


class Perceptron():
	def __init__(self, learn_rate=0.05, num_iter=10, kernel=Euclidean()):
		self.lr = learn_rate
		self.kernel = kernel
		self.num_iter = num_iter
	def fit(self, all_x, all_y):
		self.m = np.zeros((all_x.shape[0],))
        self.data = all_x
		'''
		store a vector of all zeros, as many rows as data points 
		then iter through each point. keep track of how many times it had been misclassified
		do elementwise kernel operations
		'''
		for i in range(self.num_iter):
            
			num_wrong = 0
            idx = 0
			for (x, y) in zip(all_x, all_y):
				score = np.sum(self.kernel.kernel(self.m * self.data, x))
				sign = 1 if (score > 0) else -1
				if(sign != y):
					num_wrong += 1
					self.m[idx] += 1
                idx += 1

	def classify(self, x):
		scores = self.kernel.kernel(self.w, x)
		return 1 if (np.sum(scores) > 0) else -1

	def batch_accuracy(self, x, y, verbose=False):
		num_correct = 0
		l = len(y)
		i = 0
		for x_ins, y_ins in zip(x, y):
			pred = self.classify(x_ins)
			if(verbose):
				print(f"\r{round(i / l, 3)}", end="")
			num_correct += int((pred == y_ins).all())
			i += 1
		print("")
		return num_correct / len(x)
	