import numpy as np
import math
import pdb
from collections import defaultdict

class Atom():
	'''
	Base class for representing single operations in a differentiable model.
	'''
	def __init__(self):
		'''
		Initializes internal variables required for taking gradients
		'''
		self.grad_dependencies = {}
		self.cached_grads = defaultdict(int) # defaults to 0

	def eval(self, parameters):
		'''
		Given a state dictionary, evaluate this operation.

		Parameters:
		----------
		parameters: a dictionary {String: Variable, ...} containing the values for all parameters required to evaluate this operation
		'''
		return self.numeric_eval(*[child.eval(parameters) for child in self.children])

	def numeric_eval(self, *args):
		'''
		By default, return the first argument. If this function is not overridden it is identity()

		Parameters:
		----------
		*args: Arbitrary number of inputs
		'''
		return args[0]

	def _forward_prop(self, parameters):
		'''
		Helper method that performs forward propagation. The output of each operation is stored in its cached_eval attribute.

		Parameters:
		----------
		parameters: a dictionary {String: Variable, ...} containing the values for all parameters required to evaluate this operation
		'''
		try:
			return self.cached_eval # If this node has already been evaluated, use that result
		except AttributeError:
			context = {}
			for child in self.children:
				context = {**child._forward_prop(parameters), **context}
			context[self.name] = self.numeric_eval(*[context[child.name] for child in self.children])
			self.cached_eval = context
			return self.cached_eval

	def forward_prop(self, parameters, cleanup=True):
		'''
		Calculate the ouptuts of all operations in the model. Returns a dictionary of each operation and its output. 

		Parameters:
		----------
		parameters: a dictionary {String: Variable, ...} containing the values for all parameters required to evaluate this operation
		cleanup: if True, this method will remove cached forward propagations once finished.
		'''
		self._forward_prop(self, parameters)
		result = self.cached_eval
		if(cleanup):
			self.clear_cached_eval()
		return result

	def _backprop(self, out_wrt_parent):
		'''
		Helper method that recursively performs backpropagation.
		Each child operation stores the rate of change of the root output with respect to the child operation's inputs.
		The output node is determined by parent calls to this method – see backprop().

		Parameters:
		----------
		out_wrt_parents: the gradient of the root operation's output with respect to this node's parent
		'''
		for child in self.children:
			out_wrt_child = self.grad_dependencies[child](out_wrt_parent)
			self.cached_grads[child] += out_wrt_child
			child._backprop(out_wrt_child)

	def backprop(self, parameters, cleanup=True):
		'''
		Calculate the gradient of this operation's output with respect to all child operations' inputs. If cleanup is True, node forward
		propagations will be removed once the gradients are calculated. Note that backpropagations will not be removed in either case.

		Parameters:
		----------
		parameters: a dictionary {String: Variable, ...} containing the values for all parameters required to evaluate this operation
		cleanup: if True, this method will remove cached forward propagations once finished.
		'''
		self._forward_prop(parameters)
		self._backprop(np.ones(self.shape))
		if(cleanup):
			self.clear_cached_eval()

	def grad_wrt(self, item, parameters, cleanup=True):
		'''
		Caclulate the gradient of this node with respect to a child. 
		If cleanup is True (default), removes cached forward and back propagations once finished.
		
		Parameters:
		----------
		item: The child
		parameters: a dictionary {String: Variable, ...} containing the values for all parameters required to evaluate this operation
		cleanup: if True, this method will remove cached forward and back propagations once finished.
		'''
		self.backprop(parameters, cleanup=True)
		grad = item.cached_grads[item]
		if cleanup:
			self.clear_cached_grads()
		return grad

	def clear_cached_eval(self):
		'''
		Clear the cached evaluations of this node and all children.
		'''
		self.__dict__.pop("cached_eval", None)
		[child.clear_cached_eval() for child in self.children]

	def clear_cached_grads(self):
		'''
		Clear the cached gradients of this node and all children.
		'''
		self.cached_grads = defaultdict(int)
		[child.clear_cached_grads() for child in self.children]
	
	def get_parameters(self):
		'''
		Return all parameters that are children of this model
		'''
		return sum([child.get_parameters() for child in self.children], [])
	
class BinAtom(Atom):
	def __init__(self, param_one, param_two):
		super().__init__()
		self.children = [param_one, param_two]
		self.left = param_one
		self.right = param_two

class UnAtom(Atom):
	def __init__(self, param_one):
		super().__init__()
		self.children = [param_one]
		self.center = param_one

class Variable():
	def __init__(self, name, shape):
		self.name = name
		self.shape = shape
		self.children = []
		self.cached_grads = defaultdict(int)

	def clear_cached_eval(self):
		self.__dict__.pop("cached_eval", None)
	def clear_cached_grads(self):
		self.cached_grads = defaultdict(int)

	def eval(self, parameters):
		raise NotImplemented("Can't call eval on the Variable base class")
	def _forward_prop(self, inputs):
		self.cached_eval = {self.name: self.eval(inputs)}
		return self.cached_eval
	def _backprop(self, parent_wrt_out):
		self.cached_grads[self] += parent_wrt_out
	def _grad_wrt(self, item, grad_wrt_me):
		if(self == item):
			return grad_wrt_me
		else:
			return 0

class Dot(BinAtom):
	def __init__(self, param_one, param_two):
		super().__init__(param_one, param_two)
		assert param_one.shape == param_two.shape
		self.shape = (1,)
		self.name = "{0}·{1}".format(*[child.name for child in self.children])
		self.grad_dependencies = {
			self.left: lambda parent_grad: parent_grad * self.right.cached_eval[self.right.name],
			self.right: lambda parent_grad: parent_grad * self.left.cached_eval[self.left.name]
		}
	def numeric_eval(self, x, y):
		return np.dot(x, y)

class MatMul(BinAtom):
	def __init__(self, param_one, param_two):
		super().__init__(param_one, param_two)
		assert param_one.shape[1] == param_two.shape[0]
		self.shape = (param_one.shape[0], param_two.shape[1])
		self.name = "{0} @ {1}".format(*[child.name for child in self.children])

		self.grad_dependencies = {
			self.left: lambda parent_grad: np.matmul(parent_grad, self.right.cached_eval[self.right.name].T),
			self.right: lambda parent_grad: np.matmul(self.left.cached_eval[self.left.name].T, parent_grad)
		}
	def numeric_eval(self, x, y):
		return np.matmul(x, y)

class Difference(BinAtom):
	def __init__(self, param_one, param_two):
		super().__init__(param_one, param_two)
		self.name = "{0} - {1}".format(*[child.name for child in self.children])
		if(param_one.shape == (1,)):
			self.shape = param_two.shape
		elif (param_two.shape == (1,)):
			self.shape = param_one.shape
		else:
			assert param_one.shape == param_two.shape
			self.shape = param_one.shape
		self.grad_dependencies = {
			self.left: lambda parent_grad: parent_grad,
			self.right: lambda parent_grad: -1 * parent_grad
		}
	def numeric_eval(self, x, y):
		return x - y

class Sum(BinAtom):
	def __init__(self, param_one, param_two):
		super().__init__(param_one, param_two)
		self.name = "{0} + {1}".format(*[child.name for child in self.children])
		if(param_one.shape == (1,)):
			self.shape = param_two.shape
		elif (param_two.shape == (1,)):
			self.shape = param_one.shape
		else:
			assert param_one.shape == param_two.shape
			self.shape = param_one.shape
		self.grad_dependencies = {
			self.left: lambda parent_grad: parent_grad,
			self.right: lambda parent_grad: parent_grad
		}
	def numeric_eval(self, x, y):
		return x + y

class Frac(BinAtom):
	def __init__(self, param_one, param_two):
		super().__init__(param_one, param_two)
		self.name = "{0} / {1}".format(*[child.name for child in self.children])
		if(param_one.shape == (1,)):
			self.shape = param_two.shape
		elif (param_two.shape == (1,)):
			self.shape = param_one.shape
		else:
			assert param_one.shape == param_two.shape
			self.shape = param_one.shape
		self.grad_dependencies = {
			self.left: lambda parent_grad: parent_grad / self.right.cached_eval[self.right.name],
			self.right: lambda parent_grad: self.left.cached_eval[self.left.name] * parent_grad * (-1 / (self.right.cached_eval[self.right.name]) ** 2)
		}
	def numeric_eval(self, x, y):
		return x / y

class Exp(UnAtom):
	def __init__(self, param_one, base=math.e):
		super().__init__(param_one)
		self.name = "exp({0})".format(*[child.name for child in self.children])
		self.shape = param_one.shape
		self.expt = self.center # naming for convenience
		self.base = base
		self.grad_dependencies = {
			self.expt: lambda parent_grad: parent_grad * math.log(base) * (base ** self.expt.cached_eval[self.expt.name])
		}
	def numeric_eval(self, x):
		return self.base ** x

class Const(Atom):
	def __init__(self, value):
		super().__init__()
		self.val = value
		self.name = str(value)
		self.shape = (1,)
		self.children = []
	def eval(self, parameters):
		return self.val
	def numeric_eval(self):
		return self.val

class Transpose(UnAtom):
	def __init__(self, param_one):
		super().__init__(param_one)
		self.name = "{0}^T".format(*[child.name for child in self.children])
		self.shape = (param_one.shape[1], param_one.shape[0])
		self.grad_dependencies = {
			self.center: lambda parent_grad: parent_grad.T * np.ones(self.center.shape)
		}
	def numeric_eval(self, x):
		return x.T

class MSE(BinAtom):
	def __init__(self, param_one, param_two):
		super().__init__(param_one, param_two)
		self.name = "MSE({0}, {1})".format(*[child.name for child in self.children])
		self.shape = (1,)
		self.predictor = self.left
		self.label = self.right
		self.grad_dependencies = {
			self.predictor: lambda parent_grad: parent_grad * (self.predictor.cached_eval[self.predictor.name] - self.label.cached_eval[self.label.name]),
			self.label: lambda parent_grad: parent_grad * (self.predictor.cached_eval[self.predictor.name] - self.label.cached_eval[self.label.name])
		}
	def numeric_eval(self, x, y):
		return (1/2) * (np.linalg.norm(x - y) ** 2)

class Sigmoid(UnAtom):
	def __init__(self, param_one):
		super().__init__(Frac(Exp(param_one), Sum(Const(1), Exp(param_one))))
		self.name = "σ({0})".format(param_one.name)
		self.shape = param_one.shape
		self.grad_dependencies = {
			self.center: lambda parent_grad: parent_grad
		}
		
class SumComponents(UnAtom):
	def __init__(self, param_one):
		super().__init__(param_one)
		self.name = "SumComponents({0})".format(*[child.name for child in self.children])
		self.shape = (1,)
		self.grad_dependencies = {
			self.center: lambda parent_grad: parent_grad * np.ones(self.center.shape)
		}
	def numeric_eval(self, x):
		return np.sum(x)

class Placeholder(Variable):
	def __init__(self, name, shape):
		super().__init__(name, shape)
	def eval(self, inputs):
		try:
			return inputs[self.name]
		except KeyError:
			raise Exception("The model is dependent on an input that was not provided")
	def get_parameters(self):
		return []

class Parameter(Variable):
	def __init__(self, name, init):
		super().__init__(name, init.shape)
		self.init = init
		self.val = np.copy(init)
	def eval(self, inputs):
		return self.val
	def __add__(self, other):
		if(type(other) != np.ndarray):
			raise TypeError("Cannot add {0} to parameter".format(type(other)))
		self.val += other
		return self
	def __sub__(self, other):
		if(type(other) != np.ndarray):
			raise TypeError("Cannot subtract {0} from parameter".format(type(other)))
		self.val -= other
		return self
	def reset(self):
		self.val = np.copy(self.init)
	def get_parameters(self):
		return [self]

class Model():
	def __init__(self, output, loss, instance_template=("x", "y")):
		self.output = output
		self.loss = loss
		self.instance_template = instance_template
	def train_step(self, instance, learn_rate = 0.01):
		params = self.output.get_parameters()
		grads = [self.loss.grad_wrt(param, instance) for param in params]
		for idx in range(len(params)):
			params[idx] -= learn_rate * grads[idx]
	def train_on(self, x, y, learn_rate=0.01, n_iter = 5000):
		for idx in range(n_iter):
			idx = idx % len(x)
			ins = {self.instance_template[0]: x[idx], self.instance_template[1]: y[idx]}
			self.train_step(ins, learn_rate=learn_rate)
	def mean_error(self, x, y):
		error = 0
		for obs, label in zip(x, y):
			ins = {self.instance_template[0]: obs, self.instance_template[1]: label}
			error += abs(self.loss.eval(ins))
		return error / len(x)

class Evaluator():
	def __init__(self, classifier, loss_op):
		self.classifier = classifier
		self.loss_op = loss_op
	def average_error(self, input_vecs, outputs):
		sum_error = 0
		for input_vec, output in zip(input_vecs, outputs):
			instance = {"x": input_vec, "y": output}
			sum_error += self.loss_op.eval(instance)
		return sum_error / len(input_vecs)
	
class BinaryEvaluator(Evaluator):
	def __init__(self, classifier, loss_op):
		super().__init__(classifier, loss_op)
	def correct_likelihood(self, input_vecs, outputs):
		sum_correct = 0
		for input_vec, output in zip(input_vecs, outputs):
			instance = {"x": input_vec, "y": output}
			prediction = self.classifier.eval(instance)
			if(output == 0):
				sum_correct += int(prediction < 0.5)
			else:
				sum_correct += int(prediction >= 0.5)
		return sum_correct / len(input_vecs)
