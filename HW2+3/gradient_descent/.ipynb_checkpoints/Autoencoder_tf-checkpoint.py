import tensorflow as tf
import numpy as np

# This is a reference implementation of a slightly complicated network. 
# I am using it to test the model library. 

x = tf.placeholder(tf.float32, shape=[None, 8])
y = tf.placeholder(tf.float32, shape=[None, 8])

in_hid = tf.Variable(tf.random_uniform([8, 3]))
out_hid = tf.Variable(tf.random_uniform([3, 8]))


hidden = tf.math.sigmoid(tf.linalg.matmul(x, in_hid))
#out = tf.math.sigmoid(tf.linalg.matmul(hidden, out_hid))
out_activ = tf.linalg.matmul(hidden, out_hid)



#loss = tf.losses.mean_squared_error(y, out)
loss = tf.losses.softmax_cross_entropy(y, out_activ)
optimizer = tf.train.GradientDescentOptimizer(0.1)
train_op = optimizer.minimize(loss)


tf.summary.image("in_weights", tf.reshape(in_hid, (1, 8, 3, 1)))
tf.summary.image("out_weights", tf.reshape(out_hid, (1, 3, 8, 1)))
tf.summary.image("grads", tf.reshape(tf.gradients(loss, in_hid), (1, 8, 3, 1)))

merged = tf.summary.merge_all()
writer = tf.summary.FileWriter("ae_logs/", )
# ==================================



vecs = np.identity(8)

init = tf.global_variables_initializer()
with tf.Session() as sess:
	sess.run(init)
	for step in range(100000):
		logs, _ = sess.run([merged, train_op], feed_dict={x: vecs, y: vecs})
		writer.add_summary(logs, step)
	w = sess.run([hidden], feed_dict={x: vecs})[0]
	w = (w > 0.5).astype(int)

merged = tf.summary.merge_all()

print(w)
print(len(np.unique(w, axis=0)))