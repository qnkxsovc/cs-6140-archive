# Test harnesses
import numpy as np

class Wrapper():
	'''
	Wraps a model to test it. The point of this class is to interface test harnesses with models.
	'''
	def __init__(self, model, fitter, tester):
		'''
		Encapsulates a prediction model for use in the testing systems.

		Parameters:
		----------
		- model: the model to encapsulate
		- fitter: a function fitter(x, y) that fits the model to the given observation matrix x and label vector y
		- tester: a function tester(x, y) that returns a measure of the model's goodness of fit, given test observation matrix x and label vector y
		'''
		self.model = model
		self.fitter = fitter
		self.tester = tester
	def fit(self, x, y, **kwargs):
		'''
		Use self.fitter(x, y) to fit this model

		Parameters:
		----------
		x: a matrix where columns are features and rows are observations
		y: a vector of labels
		'''
		try:
			self.fitter(x, y, **kwargs)
		except Exception as e:
			raise Exception("The fitter was not able to fit the data: {0}".format(e))
	def test(self, x, y):
		'''
		Use self.tester(x, y) to test this model. Returns the testing error.

		Parameters:
		----------
		x: a matrix where columns are features and rows are observations
		y: a vector of labels
		'''
		try:
			return self.tester(x, y)
		except Exception as e:
			raise Exception("The tester was not able to test the data: {0}".format(e))

class KFolds():
	def __init__(self, wrapper):
		'''
		Evaluate a wrapped model using k-folds

		Parameters:
		----------
		wrapper: a wrapped model to evaluate
		'''
		self.model = wrapper
	
	def testing_error(self, x, y, k=5, verbose=False, **kwargs):
		'''
		Test the model by splitting the x, y data into k parts, training on each k-1 size subset and testing on the complement.
		Returns the average testing error. 

		Parameters:
		----------
		x: a matrix where columns are features and rows are observations
		y: a vector of labels
		k: the number of folds to split the data
		verbose: if True, will pretty-print the testing process
		'''
		if verbose:
			print("Running k-folds, k={0}".format(k))
			print("=================================")

		folds = [(x[i::k], y[i::k]) for i in range(k)]
		net_error = 0

		for idx in range(k):
			test_x, test_y = folds[idx]
			train_sets = [folds[c] for c in range(k) if c != idx]
			train_x = np.concatenate([item[0] for item in train_sets])
			train_y = np.concatenate([item[1] for item in train_sets])

			if verbose:
				print("\t- Iteration {0}: fitting...".format(idx+1), end="")
			
			self.model.fit(train_x, train_y, **kwargs)

			if verbose:
				print("\r\t- Iteration {0}: testing...".format(idx+1), end="")
			
			err = self.model.test(test_x, test_y)
			net_error += err
			if verbose:
				print("\r\t- Iteration {0}: testing error {1}".format(idx+1, err))

		avg_error = net_error / k
		if verbose:
			print("Average testing error across {0} folds: {1}".format(k, avg_error))

		return avg_error
