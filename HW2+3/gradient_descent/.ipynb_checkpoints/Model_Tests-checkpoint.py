from Model import *
import math
import unittest

class TestModel(unittest.TestCase):
	def test_prod(self):
		x = Placeholder("x", (1,))
		y = Placeholder("y", (1,))
		model = Dot(x, y)
		inputs = {"x": 2, "y": 3}
		self.assertEqual(model.grad_wrt(x, inputs), y.eval(inputs))
		self.assertEqual(model.grad_wrt(y, inputs), x.eval(inputs))
	def test_frac(self):
		x = Placeholder("x", (1,))
		y = Placeholder("y", (1,))
		model = Frac(x, y)
		inputs = {"x": 2, "y": 3}
		self.assertAlmostEqual(model.grad_wrt(x, inputs), 1/3)
		self.assertAlmostEqual(model.grad_wrt(y, inputs), -2/9)
	def test_sumdiff(self):
		x = Placeholder("x", (1,))
		y = Placeholder("y", (1,))
		model_a = Sum(x, y)
		model_b = Difference(x, y)
		inputs = {"x": 2, "y": 3}
		self.assertAlmostEqual(model_a.grad_wrt(x, inputs), 1)
		self.assertAlmostEqual(model_b.grad_wrt(y, inputs), -1)
	def test_expt(self):
		x = Placeholder("x", (1,))
		model = Exp(x)
		inputs = {"x": 2}
		self.assertAlmostEqual(model.grad_wrt(x, inputs), math.e**2)
	def test_arithmetic(self):
		x = Placeholder("x", (1,))
		model = Frac(
			Exp(Difference(x, Const(1))),
			Sum(Const(1), Exp(Difference(x, Const(1)))))
		inputs = {"x": np.array([2])}

		self.assertAlmostEqual(model.eval(inputs)[0], math.e / (math.e + 1))
		self.assertAlmostEqual(model.grad_wrt(x, inputs)[0], 0.19661193324148)
		self.assertEqual(model.name, "exp(x - 1) / 1 + exp(x - 1)")
	def test_matmul(self):
		x = Placeholder("x", (2, 2))
		y = Placeholder("y", (2, 2))
		model = SumComponents(MatMul(y, x))
		inputs = {"x": np.arange(4).reshape((2, 2)), "y": np.arange(4).reshape((2, 2))}
		model.grad_wrt(x, inputs) 
		self.assertTrue((model.grad_wrt(x, inputs) == np.array([[2, 2], [4, 4]])).all())
		self.assertTrue((model.grad_wrt(y, inputs) == np.array([[1, 5], [1, 5]])).all())
	def test_transpose(self):
		x = Placeholder("x", (3, 1))
		model = MatMul(Transpose(x), x)
		inputs = {"x": np.arange(3).reshape((3, 1))}
		self.assertTrue((model.grad_wrt(x, inputs) == np.array([[0.], [2.], [4.]])).all())
	def test_mse(self):
		w = Parameter("w", np.array([0.1, 0.2, 0.3]))
		x = Placeholder("x", (3,))
		y = Placeholder("y", (1,))
		classifier = Dot(w, x)
		model = MSE(classifier, y)

		inputs = {"x": np.array([1, 1, 1]), "y": 0.5}
		self.assertTrue(np.isclose(model.grad_wrt(w, inputs), np.array([0.1, 0.1, 0.1])).all())
if __name__ == '__main__':
    unittest.main()