from collections import defaultdict, Counter
import numpy as np 
import scipy.stats
import math

'''
=== Model ===

Fields:
- self.means: dict, {class_index : mean vector}
- self.covs: dict, {class_index : covariance vector}
- self.gaussians: dict, {class_index : gaussian pdfs}

Methods:
- self.fit(x, y): takes a design matrix x and a vector of class indexes y. Fits a model.
- self.label_prediction(x, y): takes a single data vector x and a class index y. Returns the probability of y given x
- self.observation_prediction(x, y): takes a single data vector x and a class index y. Returns the probability of x given y
- self.classify(x): takes a single data vector x and returns the index of the most likely label

'''

class GDA():
	def __init__(self, shared_covariance, epsilon=0.0):
		self.shared_covariance = shared_covariance
		self.epsilon = epsilon
		self.means = -1
		self.covs = -1
		self.gaussians = -1
		self.label_distribution = -1
	def fit(self, x, y):
		sorted_data = defaultdict(list)
		self.labels = list(np.unique(y).flatten())


		for x_ins, y_ins in zip(x, y):
			# y_ins[0] unboxes y_ins from the numpy array
			sorted_data[y_ins[0]].append(x_ins)

		means_collector = {}
		for label, instances in sorted_data.items():
			means_collector[label] = np.mean(instances, axis=0)


		cov_collector = {}
		if self.shared_covariance:
			# Even when the covariance is shared, the algorithm calls for that covariance to be
			# computed with respect to each labels' mean
			for label in self.labels:
				shifted_data = x - means_collector[label]
				cov_collector[label] = np.cov(shifted_data, rowvar=False) + self.epsilon * np.identity(shifted_data.shape[1])
		else:
			for label, instances in sorted_data.items():
				shifted_data = instances - means_collector[label]
				cov_collector[label] = np.cov(shifted_data, rowvar=False) + self.epsilon * np.identity(shifted_data.shape[1])

		gaussian_collector = {}
		for label in self.labels:
			label_mean = means_collector[label]
			label_cov = cov_collector[label]
			gaussian_collector[label] = scipy.stats.multivariate_normal(mean=label_mean, cov=label_cov)

		self.means = means_collector
		self.covs = cov_collector
		self.gaussians = gaussian_collector	
		self.label_distribution = { label: freq / len(y) for label, freq in Counter(list(y.flatten())).items() }

	def label_prediction(self, x, y, log=False):
		self._assert_fit()

		p_xgy = self.observation_prediction(x, y, log=True)
		p_y = math.log(self.label_distribution[y])
		p_x = math.log(sum( [ e**( self.observation_prediction(x, y_i, log=True) + math.log(self.label_distribution[y_i])) for y_i in self.labels]))

		if(log):
			return p_xgy + p_y - p_x
		else:
			return e**(p_xgy + p_y - p_x)
	def observation_prediction(self, x, y, log=False):
		self._assert_fit()
		if(log):
			return self.gaussians[y].logpdf(x)
		else:
			return self.gaussians[y].pdf(x)

	def classify(self, x):
		score_collector = {}
		for y_i in self.labels:
			score_collector[y_i] = self.observation_prediction(x, y_i, log=True) + math.log(self.label_distribution[y_i])
		
		return max(score_collector.keys(), key = lambda y_i: score_collector[y_i])

	def batch_accuracy(self, x, y):
		num_correct = 0
		for x_ins, y_ins in zip(x, y):
			num_correct += int((self.classify(x_ins) == y_ins).all())
		return num_correct / len(x)
			
	def _assert_fit(self):
		if(self.means == -1 or self.covs == -1 or self.gaussians == -1 or self.label_distribution == -1):
			raise Error("Cannot make a prediction before the model has been fit.")

