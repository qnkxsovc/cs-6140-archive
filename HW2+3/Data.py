import numpy as np
# This is a stub file for easily loading data

def load_perceptron():
	data = np.genfromtxt("data/perceptronData.txt")
	x = data[:, :-1]
	y = data[:, -1]
	return (x, y)

def load_2gaussian():
    data = np.genfromtxt("data/2gaussian.txt")
    return data

def load_3gaussian():
    data = np.genfromtxt("data/3gaussian.txt")
    return data

def load_wine():
	data = np.genfromtxt("data/Wine.txt", delimiter=",")
	x = data[:,1:]
	y_idxs = (data[:,0] - 1).astype(np.int32)
	y = np.zeros((len(y_idxs), 3))
	y[np.arange(len(y_idxs)), y_idxs] = 1
	return (x, y)

def load_spambase():
	'''
	Returns (x, y) where x is a matrix of observations and y is a vector of labels
	'''
	data = np.genfromtxt("data/spambase.data", delimiter=",")
	x = data[:, :-1]
	y = data[:, -1]
	return (x, y)

def load_housing():
	'''
	Returns ((train_x, train_y), (test_x, test_y)) where each x is a matrix of observations and each y is a vector of labels
	'''
	train_data = np.genfromtxt("data/housing_train.txt")
	train_x = train_data[:, :-1]
	train_y = train_data[:, -1]

	test_data = np.genfromtxt("data/housing_test.txt")
	test_x = test_data[:, :-1]
	test_y = test_data[:, -1]

	return ((train_x, train_y), (test_x, test_y))

def shift_scale(observations, minim=0, maxim=1, with_params=False):
	'''
	Scales every column of observations to have the given min and max values

	Parameters:
	----------
	observations: a matrix of observations
	min: minimum value
	max: maximum value
	'''

	# to [0, 1]
	offset = observations.min(0)
	scale = 1/observations.ptp(0)
	observations = (observations - offset) * scale

	# to desired
	observations = (maxim - minim) * observations + minim
	
	if(with_params):
		return (observations, offset, scale)
	else:
		return observations

def avg_normalization(observations):
	'''
	Subtracts the mean from every column and divides by the range

	Parameters:
	----------
	observations: a matrix of observations
	'''
	return (observations - np.mean(observations, axis=0)) / observations.ptp(0)

def add_bias_dim(observations):
	'''
	Adds a column of ones as the new first column in observations

	Parameters:
	----------
	observations: a matrix of observations
	'''
	return np.concatenate((np.ones((observations.shape[0], 1)), observations), 1)

def make_test_set(x, y, test_percent=0.2):
    '''
    Takes a data and label matrix. Splits both into a testing and training set, returning four matrices.
    Parameters:
    ----------
    x: a data matrix where rows are instances and columns are features
    y: a label matrix where rows are instances and columns yield a label
    test_percent: the percent of input data to use for testing. This is approximate due to integer rounding.
    '''
    shuffler = np.random.permutation(len(x))
    split_idx = int(len(x) * test_percent)
    test = (x[shuffler[:split_idx]], y[shuffler[:split_idx]])
    train = (x[shuffler[split_idx:]], y[shuffler[split_idx:]])
    return (train, test)