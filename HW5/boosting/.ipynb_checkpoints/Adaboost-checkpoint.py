import math
import numpy as np
from multiprocessing import Pool
from collections import defaultdict 

def _hamming_distance(x, y):
	dif = np.abs(x - y)
	return np.sum(dif) / len(dif)

class ECOC():
	def __init__(self):
		self.d_encoder = -1
		self.d_decoder = -1
		self.adaboosters = -1
		self.num_labels = -1
		self.num_bits = -1
		self.bitstrings = -1
		self.int_bitstrings = -1
	def fit(self, all_x, all_y, num_stumps=100, bitstr_len=20, stumps="random"):
		labels = np.unique(all_y)
		self.num_labels = len(labels)

		self._make_coders(labels, bitstr_len)
		bitstrs = self.bitstrings
		self.num_bits = bitstr_len

		label_groups = [bitstrs[:, i][all_y] for i in range(bitstr_len)] # store groups of labels for training each adabooster

		def train_booster(args):
			x_features, y_bin, index = args
			print(index)
			y_bin[y_bin == 0] = -1
			a = Adaboost(stumps)
			a.fit(x_features, y_bin, 600)
			return (a, index)

		batch_args = [(all_x, label_groups[i], i) for i in range(bitstr_len)]

		adaboosters = map(train_booster, batch_args)
		self.adaboosters = { bt: booster for booster, bt in adaboosters }

	def _make_coders(self, labels, codeword_size):
		# goal: generate len(labels) number of codewords each with codeword_size bits
		# do so by randomly selecting binary strings without replacement
		bit_strs = np.random.randint(0, 2, size=(len(labels), codeword_size))
		int_strs = [ sum([row[i] * 2**i for i in range(len(row))]) for row in bit_strs]
		self.d_encoder = { idx: bit_strs[idx,:] for idx in range(len(labels))}
		self.d_decoder = { int_strs[idx] : idx for idx in range(len(labels))}
		self.bitstrings = bit_strs
		self.int_bitstrings = int_strs

	def batch_accuracy(self, x, y):
		self._assert_fit()
		num_correct = 0
		for x_ins, y_ins in zip(x, y):
			c = self.classify(x_ins)
			num_correct += int(c == y_ins)
		return num_correct / len(x)
	
	def classify(self, x):
		self._assert_fit()

		raw_classification = np.array([self.adaboosters[idx].classify(x) for idx in range(self.num_bits)])
		raw_classification[raw_classification == -1] = 0

		best_hd = math.inf 
		best_bitstr = -1
		for bitstr in self.bitstrings:
			hd = _hamming_distance(bitstr, raw_classification)
			if  hd < best_hd:
				best_bitstr = bitstr
				best_hd = hd
		best_intstr = sum([best_bitstr[i] * 2**i for i in range(len(best_bitstr))])
		return self.d_decoder[best_intstr]

	def _assert_fit(self):
		if(self.d_encoder == -1 and self.d_decoder == -1 and self.adaboosters == -1 and  self.bitstrings == -1):
			raise Exception("The model must be fit before it can be trained.")

class Adaboost():
	def __init__(self, stumps="best"):
		'''

		:param stumps: { "best", "random", "best_inexact" }
			Set to "best" to have the best stump selected at each iteration.
			Set to "random" to have random stumps selected.
			Set to "best_inexact" to pick the best of a limited range of options.
		'''
		self.alphas = -1
		self.stumps = -1
		self.stump_type = stumps
		self.stump_diagnostics = -1
	def avg_margin_per_feature(self, all_x, all_y):
		self._assert_fit()

		def ev_st(x, f, t):
			return 1 if x[f] > t else -1

		sum_margin = np.sum(np.abs(self.alphas))
		confidences = defaultdict(int)
		stumps_per_feature = defaultdict(list)
		for i in range(len(self.alphas)):
			f, t = self.stump_diagnostics[i]
			a = self.alphas[i]
			confidences[f] += abs(a)
			stumps_per_feature[f].append((a, t))
		confidences = { f: m / sum_margin for f, m in confidences.items() }

		cond_margins = { f: sum([ev_st(x, f, t) * a * y for x, y in zip(all_x, all_y) for a, t in st]) for f, st in stumps_per_feature.items()}
		net_margin = sum(list(cond_margins.values()))
		cond_margins = { f: a / net_margin for f, a in cond_margins.items() }

		return { f: confidences[f] * cond_margins[f] for f in cond_margins.keys() }

	def fit(self, all_x, all_y, num_stumps=100, bins=10):
		# First: make an initial distribution 1/n for every point in all_x
		# Then pass the distribution and all x and all y to find_best_stump
		# then record the best stump and its associated alpha
		# do this for the required number of stumps

		# !!! This currently does not handle the case when many observations have the same feature value
		# !!! There also seems to be problems in the error calculations

		all_y_for_comp = np.copy(all_y)
		all_y_for_comp[all_y == -1] = 0
		all_y_for_comp = all_y_for_comp.astype(np.bool)

		def find_random_stump(dist):
			r_feature = np.random.randint(all_x.shape[1])
			r_threshold = np.random.choice(np.unique(all_x[:,r_feature]))
			split = lambda item, k_=r_feature, t_=r_threshold: 1 if item[k_] > t_ else -1

			error = np.sum(dist[all_y_for_comp != (all_x[:, r_feature] > r_threshold)])
			perf = abs(1/2 - error)
			best_split_diagnostics = (r_feature, r_threshold)
			return (split, perf, error, best_split_diagnostics)

		def find_best_inexact(dist, bins=10):
			best_splits = []

			for k in range(all_x.shape[1]):
				best_split, best_perf = -math.inf, -math.inf
				best_error = -1

				features = all_x[:, k]
				thresh_candidates = np.linspace(np.min(features), np.max(features), num=bins)
				for c in thresh_candidates:
					classified_pos_idxs = features > c
					actually_neg_idxs = all_y == -1
					false_pos = np.sum(dist[ np.logical_and(classified_pos_idxs, actually_neg_idxs)])
					false_neg = np.sum(dist[ ~ np.logical_or(classified_pos_idxs, actually_neg_idxs)])

					error_rate = false_pos + false_neg
					perf = abs(1/2 - error_rate)
					if(perf > best_perf):
						best_perf = perf
						best_split_diagnostics = (k, c)
						best_split = lambda item, k_=k, c_ = c: 1 if item[k_] > c_ else -1
						best_error = error_rate
				best_splits.append((best_split, best_perf, best_error, best_split_diagnostics))
			return max(best_splits, key=lambda i: i[1])

		def find_best_stump(dist):
			sorter = np.argsort(all_x, axis=0)
			best_splits = []
			neg_dist_sum = np.sum(dist[all_y == -1])
			for k in range(all_x.shape[1]):
				best_split, best_perf = -math.inf, -math.inf
				best_split_diagnostics = -1
				best_error = -1
				# starts with a neg and pos child, keeping the Sum Distribution and
				# the Sum Distribution of 1 labeled items

				neg_sd1 = 0
				pos_sd0 = neg_dist_sum

				for data_idx in range(len(all_x) - 1):
					idx = sorter[data_idx, k]
					next_idx = sorter[data_idx + 1, k]
					x, y, d = all_x[idx], all_y[idx], dist[idx]
					next_x = all_x[next_idx]

					if(y == 1):
						neg_sd1 += d
					else:
						pos_sd0 -= d

					if(x[k] != next_x[k]):
						perf = abs(1/2 - (neg_sd1 + pos_sd0)) # this is designed to be maximized

						if(perf > best_perf):
							best_perf = perf
							t = all_x[idx][k]
							best_split_diagnostics = (k, t)
							best_split = lambda item, k_=k, t_=t: 1 if item[k_] > t_ else -1
							best_error = (neg_sd1 + pos_sd0)

				best_splits.append((best_split, best_perf, best_error, best_split_diagnostics))
			return max(best_splits, key=lambda i: i[1])


		d = np.full(len(all_x), 1/len(all_x))

		self.alphas = []
		self.stumps = []
		self.stump_diagnostics = []

		for n in range(num_stumps):
			split, perf, error, diag = -1, -1, -1, -1
			if(self.stump_type == "best"):
				split, perf, error, diag = find_best_stump(d)
			elif(self.stump_type == "best_inexact"):
				split, perf, error, diag = find_best_inexact(d, bins)
			elif(self.stump_type == "random"):
				split, perf, error, diag = find_random_stump(d)

			a_t = math.log((1 - error) / error) / 2
			signs = - (all_y * np.apply_along_axis(split, 1, all_x))
			upd = math.e**(signs * a_t)
			d = d*upd
			d = d / np.sum(d)
			self.alphas.append(a_t)
			self.stumps.append(split)
			self.stump_diagnostics.append(diag)

	def score(self, x):
		self._assert_fit()
		score = np.sum([self.alphas[i] * self.stumps[i](x) for i in range(len(self.alphas))])
		return score
	def classify(self, x):
		self._assert_fit()
		return 1 if self.score(x) > 0 else -1
	def batch_accuracy(self, x, y):
		self._assert_fit()
		num_correct = 0
		for x_ins, y_ins in zip(x, y):
			num_correct += int((self.classify(x_ins) == y_ins).all())
		return num_correct / len(x)
	def _assert_fit(self):
		if(self.alphas == -1 and self.stumps == -1):
			raise Exception("Model cannot be used before it is fit to data")

