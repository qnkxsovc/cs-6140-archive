from Model import *
from Controller import *
import unittest


class TestController(unittest.TestCase):
	def setUp(self):
		# Generate an easy binary classification data set
		neg_examples = np.random.normal(loc=0.0, size=(100, 4))
		neg_labels = np.zeros((100, 1))
		pos_examples = np.random.normal(loc=10.0, size=(100, 4))
		pos_labels = np.ones((100, 1))
		self.train_x = np.concatenate((neg_examples, pos_examples), axis=0)
		self.train_y = np.concatenate((neg_labels, pos_labels), axis=0)

	def test_grad_descent(self):
		x = Placeholder("x", (1, 4))
		y = Placeholder("y", (1,))
		w = Parameter("w", np.random.normal(size=(1, 4)))
		b = Parameter("b", np.random.normal(size=(1,)))
		predictor = Sigmoid(Sum(Dot(w, x), b))
		loss = MSE(predictor, y)
		model = DFBuilder().loss(loss).input(x).output(predictor).target(y).compile()

		model.gradient_descent(self.train_x, self.train_y, n_iter=2000)
		classifications = (np.array(model.eval(self.train_x)) > 0.5).astype(int)
		wrong = np.sum(np.absolute(classifications - self.train_y))
		self.assertTrue(wrong == 0)
	'''
	def test_newton_method(self):
		x = Placeholder("x", (1, 4))
		y = Placeholder("y", (1,))
		w = Parameter("w", np.random.normal(size=(1, 4)))
		b = Parameter("b", np.random.normal(size=(1,)))
		predictor = Sigmoid(Sum(Dot(w, x), b))
		loss = MSE(predictor, y)
		model = DFBuilder().loss(loss).input(x).output(predictor).target(y).compile()

		model.newton_method(self.train_x, self.train_y, n_iter=20)
		classifications = (np.array(model.eval(self.train_x)) > 0.5).astype(int)
		wrong = np.sum(np.absolute(classifications - self.train_y))
		self.assertTrue(wrong == 0)
	'''
if __name__ == '__main__':
    unittest.main()