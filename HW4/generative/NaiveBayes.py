from collections import defaultdict, Counter
import numpy as np 
import scipy.stats
import math

'''
=== Model ===

Fields:
- self.means: dict, {class_index : mean vector}
- self.covs: dict, {class_index : covariance vector}
- self.gaussians: dict, {class_index : gaussian pdfs}

Methods:
- self.fit(x, y): takes a design matrix x and a vector of class indexes y. Fits a model.
- self.label_prediction(x, y): takes a single data vector x and a class index y. Returns the probability of y given x
- self.observation_prediction(x, y): takes a single data vector x and a class index y. Returns the probability of x given y
- self.classify(x): takes a single data vector x and returns the index of the most likely label
'''

class _Bins():
	def __init__(self, bin_range=None, count=None, bins=None, laplace=False):
		self.laplace = laplace
		self.bins = bins
		self.range = bin_range
		self.count = count

	def _interpolator(self, lower, upper, count, precision=float):
		r = upper - lower
		if(precision == float):
			interval_size = r / count
			return [ lower + (interval_size * i) for i in range(count) ]
		elif(precision == int):
			interval_size = r // count
			return [lower + (interval_size * i) for i in range(count)]
	def populate(self, x):
		if(self.bins == None):
			sort_idxs = np.argsort(x)
			s_x = np.array(x)[sort_idxs]
			lower = s_x[0]
			upper = s_x[-1]
			bins = self._interpolator(lower, upper, self.count, precision=float)
			
			bins_collector = defaultdict(int, {b: 0 for b in bins})
			for item in s_x:
				i = 0
				while(i < (len(bins)-1) and item > bins[i]):
					i += 1

				bins_collector[bins[i]] += 1

			if(self.laplace):
				bins_collector = {label : (freq + 1)/ (len(x) + len(bins)) for label, freq in bins_collector.items()}
			else:
				bins_collector = {label : freq / len(x) for label, freq in bins_collector.items()}


			self.bins = bins
			self.binned_freqs = bins_collector
		else:
			sort_idxs = np.argsort(x)
			s_x = np.array(x)[sort_idxs]
			lower = s_x[0]
			upper = s_x[-1]
			bins_collector = defaultdict(int, {b: 0 for b in self.bins})

			for item in s_x:
				i = 0
				while(i < (len(self.bins)-1) and item > self.bins[i]):
					i += 1
				bins_collector[self.bins[i]] += 1

			if(self.laplace):
				bins_collector = {label : (freq + 1)/ (len(x) + len(self.bins)) for label, freq in bins_collector.items()}
			else:
				bins_collector = {label : freq / len(x) for label, freq in bins_collector.items()}

			self.binned_freqs = bins_collector
	def locate_in_bin(self, x):
		i = 0
		while(i < (len(self.bins)-1) and x > self.bins[i]):
			i += 1
		return self.binned_freqs[self.bins[i]]


class Histograms():
	def __init__(self, count=None, laplace=False, bins = None):
		self.count = count
		self.bins = None
		self.labels = -1
		self.label_distribution = -1
		self.laplace = laplace
		self.bins = bins

	def fit(self, x, y):
		self.labels = list(np.unique(y).flatten())

		sorted_data = defaultdict(list)
		for x_ins, y_ins in zip(x, y):
			# y_ins[0] unboxes y_ins from the numpy array
			sorted_data[y_ins[0]].append(x_ins)

		self.label_distribution = { label: freq / len(y) for label, freq in Counter(list(y.flatten())).items() }

		labelled_bins_collector = {}
		for label, x_vals in sorted_data.items():
			r = (np.min(x_vals), np.max(x_vals))
			label_bin = _Bins(bin_range=r, count=self.count, laplace = self.laplace, bins = self.bins)
			label_bin.populate(x_vals)
			labelled_bins_collector[label] = label_bin

		self.bins = labelled_bins_collector

	def label_prediction(self, x, y, log=False):
		self._assert_fit()

		p_xgy = self.observation_prediction(x, y, log=True)
		p_y = math.log(self.label_distribution[y])
		p_x = math.log(sum( [ math.e**( self.observation_prediction(x, y_i, log=True) + math.log(self.label_distribution[y_i])) for y_i in self.labels]))

		if(log):
			return p_xgy + p_y - p_x
		else:
			return math.e**(p_xgy + p_y - p_x)

	def observation_prediction(self, x, y, log=False):
		self._assert_fit()

		l = self.bins[y].locate_in_bin(x)

		logpdf = -math.inf
		if(l != 0):
			logpdf = np.log(l)

		if(log):
			return logpdf
		else:
			return math.e**logpdf

	def classify(self, x):
		score_collector = {}
		for y_i in self.labels:
			score_collector[y_i] = self.observation_prediction(x, y_i, log=True) + math.log(self.label_distribution[y_i])
		return max(score_collector.keys(), key = lambda y_i: score_collector[y_i])
		
	def _assert_fit(self):
		if(self.bins == -1):
			raise Exception("Cannot make a prediction before the model has been fit.")
			
class Bernoulli():
	def __init__(self, laplace=False):
		self.mean = -1
		self.likelihoods = -1
		self.labels = -1
		self.label_distribution = -1
		self.laplace = laplace
	def fit(self, x, y):
		sorted_data = defaultdict(list)
		self.labels = list(np.unique(y).flatten())

		for x_ins, y_ins in zip(x, y):
			# y_ins[0] unboxes y_ins from the numpy array
			sorted_data[y_ins[0]].append(x_ins)

		self.label_distribution = { label: freq / len(y) for label, freq in Counter(list(y.flatten())).items() }

		means_collector = {}
		likelihoods_collector = {}
		for label, instances in sorted_data.items():
			label_mean = np.mean(instances, axis=0)
			means_collector[label] = label_mean

			success_likelihoods = 0
			if self.laplace:
				success_likelihoods = (len([ i for i in instances if i > label_mean] ) + 1) / (len(instances) + len(self.labels))
			else:
				success_likelihoods = len([ i for i in instances if i > label_mean] ) / len(instances) 

			likelihoods_collector[label] = success_likelihoods

		self.mean = means_collector
		self.likelihoods = likelihoods_collector

	def label_prediction(self, x, y, log=False):
		self._assert_fit()

		p_xgy = self.observation_prediction(x, y, log=True)
		p_y = math.log(self.label_distribution[y])
		p_x = math.log(sum( [ math.e**( self.observation_prediction(x, y_i, log=True) + math.log(self.label_distribution[y_i])) for y_i in self.labels]))

		if(log):
			return p_xgy + p_y - p_x
		else:
			return math.e**(p_xgy + p_y - p_x)

	def observation_prediction(self, x, y, log=False):
		self._assert_fit()

		mean = self.mean[y]
		l = self.likelihoods[y] if x > mean else 1 - self.likelihoods[y]

		logpdf = -math.inf
		if(l != 0):
			logpdf = np.log(l)
		if(log):
			return logpdf
		else:
			return math.e**logpdf

	def classify(self, x):
		score_collector = {}
		for y_i in self.labels:
			score_collector[y_i] = self.observation_prediction(x, y_i, log=True) + math.log(self.label_distribution[y_i])
		return max(score_collector.keys(), key = lambda y_i: score_collector[y_i])
		
	def _assert_fit(self):
		if(self.mean == -1 and self.var == -1):
			raise Exception("Cannot make a prediction before the model has been fit.")

class UnivariateGaussian():
	def __init__(self, epsilon=0.0):
		self.means = -1
		self.vars = -1
		self.labels = -1
		self.label_distribution = -1
		self.epsilon = epsilon
	def fit(self, x, y):
		sorted_data = defaultdict(list)
		self.labels = list(np.unique(y).flatten())

		for x_ins, y_ins in zip(x, y):
			# y_ins[0] unboxes y_ins from the numpy array
			sorted_data[y_ins[0]].append(x_ins)

		self.label_distribution = { label: freq / len(y) for label, freq in Counter(list(y.flatten())).items() }

		means_collector = {}
		vars_collector = {}
		for label, instances in sorted_data.items():
			means_collector[label] = np.mean(instances, axis=0)

			var = np.var(instances, axis=0)
			if(var < self.epsilon):
				var = self.epsilon
			vars_collector[label] = var

		self.means = means_collector
		self.vars = vars_collector

	def label_prediction(self, x, y, log=False):
		self._assert_fit()

		p_xgy = self.observation_prediction(x, y, log=True)
		p_y = math.log(self.label_distribution[y])
		p_x = math.log(sum( [ math.e**( self.observation_prediction(x, y_i, log=True) + math.log(self.label_distribution[y_i])) for y_i in self.labels]))

		if(log):
			return p_xgy + p_y - p_x
		else:
			return math.e**(p_xgy + p_y - p_x)

	def observation_prediction(self, x, y, log=False):
		self._assert_fit()
		logpdf = math.log(1 / (( 2 * math.pi * self.vars[y]) ** 1/2)) + (-1 * (x - self.means[y])**2) / (2 * self.vars[y])
		if(log):
			return logpdf
		else:
			return math.e**logpdf

	def classify(self, x):
		score_collector = {}
		for y_i in self.labels:
			score_collector[y_i] = self.observation_prediction(x, y_i, log=True) + math.log(self.label_distribution[y_i])
		return max(score_collector.keys(), key = lambda y_i: score_collector[y_i])
		
	def _assert_fit(self):
		if(self.means == -1 and self.var == -1):
			raise Exception("Cannot make a prediction before the model has been fit.")

class NaiveBayes():
	def __init__(self, univ_model_gen):
		self.univ_model_gen = univ_model_gen
		self.univ_models = -1 
		self.labels = -1
		self.dim = -1
		self.label_distribution = -1
	def fit(self, x, y):
		self.labels = list(np.unique(y).flatten())
		self.dim = x.shape[1]
		# for each label, for each feature, make a distribution

		models_collector = {}

		for idx in range(self.dim):
			data = x[:, idx]
			univ_model = self.univ_model_gen()
			univ_model.fit(data, y)
			models_collector[idx] = univ_model

		self.univ_models = models_collector
		self.label_distribution = { label: freq / len(y) for label, freq in Counter(list(y.flatten())).items() }


	def label_prediction(self, x, y, log=False):
		self._assert_fit()

		p_xgy = self.observation_prediction(x, y, log=True)
		p_y = math.log(self.label_distribution[y])
		try:
			p_x = math.log(sum( [ math.e**( self.observation_prediction(x, y_i, log=True) + math.log(self.label_distribution[y_i])) for y_i in self.labels]))
		except:
			if(y == max([self.observation_prediction(x, y_i, log=True) + math.log(self.label_distribution[y_i]) for y_i in self.labels])):
				return np.array(1)
			else:
				return np.array(0)

		if(log):
			return p_xgy + p_y - p_x
		else:
			return math.e**(p_xgy + p_y - p_x)

	def observation_prediction(self, x, y, log=False):
		self._assert_fit()
		logpdf = sum( [self.univ_models[i].observation_prediction(x[i], y, log=True) for i in range(self.dim)])
		if(log):
			return logpdf
		else:
			return math.e**logpdf

	def classify(self, x):
		score_collector = {}
		for y_i in self.labels:
			score_collector[y_i] = self.observation_prediction(x, y_i, log=True) + math.log(self.label_distribution[y_i])
		return max(score_collector.keys(), key = lambda y_i: score_collector[y_i])
	
	def batch_accuracy(self, x, y):
		num_correct = 0
		for x_ins, y_ins in zip(x, y):
			num_correct += int((self.classify(x_ins) == y_ins).all())
		return num_correct / len(x)

	def _assert_fit(self):
		if(self.univ_models == -1 and self.labels == -1 and self.dim == -1):
			raise Exception("Cannot make a prediction before the model has been fit.")