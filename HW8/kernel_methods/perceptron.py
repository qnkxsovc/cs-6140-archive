import numpy as np

class RBF():
	def __init__(self, sigma=1):
		self.sigma = sigma
	def kernel(self, known_data, test_ins):
		euc = np.sum( (known_data - test_ins.reshape((1, -1)))**2, axis=1)
		v = (-1 / (2 * self.sigma)) * euc
		k = np.exp(v)
		return k

class Polynomial():
	def __init__(self, p=2, c=-1):
		self.p = p
		self.c = c
	def kernel(self, known_data, test_ins):
		k = known_data @ test_ins.reshape((-1, 1))
		return (k.flatten() + self.c) ** self.p

class Euclidean():
	def __init__(self):
		pass
	def kernel(self, known_data, test_ins):
		scores = np.sqrt( np.sum( (known_data - test_ins.reshape((1, -1)))**2, axis=1) )
		return -1 * scores

class Cosine():
	def __init__(self):
		pass
	def kernel(self, known_data, test_ins):
		known_data = known_data / np.linalg.norm(known_data, axis=1).reshape((-1, 1))
		return (known_data @ (test_ins / np.linalg.norm(test_ins))).reshape((-1, 1)).flatten()


class Perceptron():
	def __init__(self, learn_rate=1, n_iter=10, kernel=Euclidean()):
		self.lr = learn_rate
		self.kernel = kernel
		self.num_iter = n_iter
	def fit(self, all_x, all_y):
		self.w = np.random.random(size=(1, all_x.shape[1]))
		self.signs = np.array([1])
		'''
		store a vector of all zeros, as many rows as data points 
		then iter through each point. keep track of how many times it had been misclassified
		do elementwise kernel operations
		'''
		for i in range(self.num_iter):
			num_wrong = 0
			for x, y in zip(all_x, all_y):
				score = np.sum(self.signs * self.kernel.kernel(self.w, x))
				sign = 1 if (score > 0) else -1
				if(sign != y):
					num_wrong += 1
					self.w = np.concatenate([self.w, (self.lr * x).reshape((1, -1))], axis=0)
					self.signs = np.concatenate([self.signs, [y] ], axis=0)

	def classify(self, x):
		scores = self.signs * self.kernel.kernel(self.w, x)
		s = np.sum(scores)
		return 1 if (s > 0) else -1

	def batch_accuracy(self, x, y, verbose=False):
		num_correct = 0
		l = len(y)
		i = 0
		for x_ins, y_ins in zip(x, y):
			pred = self.classify(x_ins)
			if(verbose):
				print(f"\r{round(i / l, 3)}", end="")
			num_correct += int((pred == y_ins).all())
			i += 1
		print("")
		return num_correct / len(x)
	