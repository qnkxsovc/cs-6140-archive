import numpy as np
import os
from collections import Counter
import mnist
# This is a stub file for easily loading data

def load_haar_mnist():
    train_x = np.load("data/haar_mnist/train_haar_features.npy")
    train_y = np.load("data/haar_mnist/train_labels.npy")
    test_x = np.load("data/haar_mnist/test_haar_features.npy")
    test_y = np.load("data/haar_mnist/test_labels.npy")
    return ((train_x, train_y), (test_x, test_y))

def load_mnist():
	#(train_imgs, train_labels, test_imgs, test_labels)
	train_imgs, train_labels, test_imgs, test_labels = mnist.load()
	train_imgs, test_imgs = train_imgs.reshape((-1, 28, 28)), test_imgs.reshape((-1, 28, 28))
	train_labels, test_labels = train_labels.astype(np.int8), test_labels.astype(np.int8)
	return (train_imgs, train_labels, test_imgs, test_labels)

def load_svm_toy():
	test_x = np.genfromtxt("data/svm_toy_dataset/test_x.csv", delimiter=",")
	test_y = np.genfromtxt("data/svm_toy_dataset/test_y.csv", delimiter=",")
	train_x = np.genfromtxt("data/svm_toy_dataset/train_x.csv", delimiter=",")
	train_y = np.genfromtxt("data/svm_toy_dataset/train_y.csv", delimiter=",")

	return ((train_x, train_y), (test_x, test_y))

def load_newsgroup():
	def _load_part(path):
		with open(os.path.join(path, "config.txt")) as cfg:
			cfg = [ x.split("=") for x in cfg.readlines()]
			num_classes = int(cfg[2][1])
			num_ins = int(cfg[3][1])
			num_feat = int(cfg[4][1])
		mat = np.zeros((num_ins, num_feat))
		labels = np.zeros((num_ins,))
		with open(os.path.join(path, "feature_matrix.txt")) as data:
			for idx, ins in enumerate(data.readlines()):
				features = ins.split(" ")
				label = int(features[0])
				coords = [x.split(":") for x in features[1:] if x != "\n"]
				for coord in coords:
					mat[idx][int(coord[0])] = float(coord[1])

				labels[idx] = label
		labels = labels.astype(np.int32)
		return mat, labels
	train_x, train_y = _load_part("data/Newsgroup8/train.trec")
	test_x, test_y = _load_part("data/Newsgroup8/test.trec")
	return ((train_x, train_y), (test_x, test_y))
	
def load_vote():
	config_path = "data/vote/vote.config"
	data_path = "data/vote/vote.data"

	with open(config_path) as cfg_f:
		cfg = [line.split("\t") for line in cfg_f.readlines()][:-1]
	with open(data_path) as data_f:
		data = [line.split("\t") for line in data_f.readlines()]


	config_dict = {
		"count": int(cfg[0][0]),
		"num_features": int(cfg[0][1]) + int(cfg[0][2]),
	}
	for f_idx, feature in enumerate(cfg[1:-1]):
		num_items = int(feature[0])
		coder = {"y": [1], "n": [0]}
		most_frequent = Counter([d_ins[f_idx] for d_ins in data]).most_common(1)[0][0]
		coder["?"] = coder[most_frequent]
		config_dict[f_idx] = {"type": str,
								"num_items": num_items,
								"options": ["y", "n"],
								"coder": coder }
	label_options = [l.strip() for l in cfg[-1][1:]]

	config_dict[config_dict["num_features"]] = {"type": str,
	                                            "num_items": 2,
	                                            "options": label_options,
	                                            "coder": {label_options[0]: [1], label_options[1]: [0]}}

	data_bin = []
	for d_idx, d in enumerate(data):
		vec = np.array([])
		for f_idx, f in enumerate(d):
			f = f.strip()
			v = config_dict[f_idx]["coder"][f]
			vec = np.concatenate((vec, v))
		data_bin.append(vec)

	data_mat = np.vstack(data_bin)
	all_x = data_mat[:, :-1]
	all_y = data_mat[:, -1]
	return (all_x, all_y, config_dict)

def load_crx():

	config_path = "data/crx/crx.config"
	data_path = "data/crx/crx.data"

	def make_onehot(discrete_values):
		count = len(discrete_values)
		encoding = np.identity(count)
		return { discrete_values[i] : encoding[i,:] for i in range(count) }

	with open(config_path) as cfg_f:
		cfg = [line.split("\t") for line in cfg_f.readlines()][:-1]
	with open(data_path) as data_f:
		data = [line.split("\t") for line in data_f.readlines()]
	config_dict = {
		"count": int(cfg[0][0]),
		"num_features": int(cfg[0][1]) + int(cfg[0][2]),
		"discrete": cfg[0][1],
		"continuous": cfg[0][2],
	}
	for f_idx, feature in enumerate(cfg[1:-1]):
		if(feature[0].strip() == "-1000"):
			config_dict[f_idx] = {"type": float }
			present = [float(d_ins[f_idx]) for d_ins in data if d_ins[f_idx] != "?"]
			mean = sum(present) / len(present)
			config_dict[f_idx]["mean"] = mean

		else:
			num_items = int(feature[0])
			options = [o.strip() for o in feature[1:]]
			onehots = make_onehot(options)
			most_frequent = Counter([d_ins[f_idx] for d_ins in data]).most_common(1)[0][0]
			onehots["?"] = onehots[most_frequent]
			config_dict[f_idx] = {"type": str,
			                        "num_items": num_items,
			                        "options": options,
			                        "coder": onehots }
	label_options = [l.strip() for l in cfg[-1][1:]]

	#label:
	config_dict[config_dict["num_features"]] = {"type": str,
	                "num_items": 2,
	                "options": label_options,
	                "coder": { label_options[0] : [1], label_options[1] : [0] }}

	data_bin = []
	for d_idx, d in enumerate(data):
		vec = np.array([])
		for f_idx, f in enumerate(d):
			f = f.strip()
			if(config_dict[f_idx]["type"] == float):
				if(f == "?"):
					vec = np.concatenate((vec, [config_dict[f_idx]["mean"]]))
				else:
					vec = np.concatenate((vec, [float(f)]))
			else:
				v = config_dict[f_idx]["coder"][f]
				vec = np.concatenate((vec, v))
		data_bin.append(vec)

	data_mat = np.vstack(data_bin)
	all_x = data_mat[:, :-1]
	all_y = data_mat[:, -1]
	return (all_x, all_y, config_dict)

def load_p_spambase():
    test_x = np.genfromtxt("data/spam_polluted/test_feature.txt")
    test_y = np.genfromtxt("data/spam_polluted/test_label.txt")
    train_x = np.genfromtxt("data/spam_polluted/train_feature.txt")
    train_y = np.genfromtxt("data/spam_polluted/train_label.txt")
    return ((train_x, train_y), (test_x, test_y))

def load_perceptron():
	data = np.genfromtxt("data/perceptronData.txt")
	x = data[:, :-1]
	y = data[:, -1]
	return (x, y)

def load_2gaussian():
    data = np.genfromtxt("data/2gaussian.txt")
    return data

def load_3gaussian():
    data = np.genfromtxt("data/3gaussian.txt")
    return data

def load_wine():
	data = np.genfromtxt("data/Wine.txt", delimiter=",")
	x = data[:,1:]
	y_idxs = (data[:,0] - 1).astype(np.int32)
	y = np.zeros((len(y_idxs), 3))
	y[np.arange(len(y_idxs)), y_idxs] = 1
	return (x, y)

def load_spambase():
	'''
	Returns (x, y) where x is a matrix of observations and y is a vector of labels
	'''
	data = np.genfromtxt("data/spambase.data", delimiter=",")
	x = data[:, :-1]
	y = data[:, -1]
	return (x, y)

def load_housing():
	'''
	Returns ((train_x, train_y), (test_x, test_y)) where each x is a matrix of observations and each y is a vector of labels
	'''
	train_data = np.genfromtxt("data/housing_train.txt")
	train_x = train_data[:, :-1]
	train_y = train_data[:, -1]

	test_data = np.genfromtxt("data/housing_test.txt")
	test_x = test_data[:, :-1]
	test_y = test_data[:, -1]

	return ((train_x, train_y), (test_x, test_y))

def var_norm(observations):
	mean = np.mean(observations, axis=0)
	var = np.var(observations, axis=0)
	return (observations - mean) / np.sqrt(var)

def shift_scale(observations, minim=0, maxim=1, with_params=False):
	'''
	Scales every column of observations to have the given min and max values

	Parameters:
	----------
	observations: a matrix of observations
	min: minimum value
	max: maximum value
	'''

	# to [0, 1]
	offset = observations.min(0)
	scale = 1/observations.ptp(0)
	observations = (observations - offset) * scale

	# to desired
	observations = (maxim - minim) * observations + minim
	
	if(with_params):
		return (observations, offset, scale)
	else:
		return observations

def avg_normalization(observations):
	'''
	Subtracts the mean from every column and divides by the range

	Parameters:
	----------
	observations: a matrix of observations
	'''
	return (observations - np.mean(observations, axis=0)) / observations.ptp(0)

def add_bias_dim(observations):
	'''
	Adds a column of ones as the new first column in observations

	Parameters:
	----------
	observations: a matrix of observations
	'''
	return np.concatenate((np.ones((observations.shape[0], 1)), observations), 1)

def make_test_set(x, y, test_percent=0.2):
    '''
    Takes a data and label matrix. Splits both into a testing and training set, returning four matrices.
    Parameters:
    ----------
    x: a data matrix where rows are instances and columns are features
    y: a label matrix where rows are instances and columns yield a label
    test_percent: the percent of input data to use for testing. This is approximate due to integer rounding.
    '''
    shuffler = np.random.permutation(len(x))
    split_idx = int(len(x) * test_percent)
    test = (x[shuffler[:split_idx]], y[shuffler[:split_idx]])
    train = (x[shuffler[split_idx:]], y[shuffler[split_idx:]])
    return (train, test)

def load_spirals():
	data = np.genfromtxt("data/twoSpirals.txt")
	x = data[:, :-1]
	y = data[:, -1]
	return (x, y)
