import numpy as np
from collections import Counter

class Polynomial():
	def __init__(self, p=2, c=-1):
		self.p = p
		self.c = c
	def kernel(self, known_data, test_ins):
		k = known_data @ test_ins.reshape((-1, 1))
		return (k.flatten() + self.c) ** self.p

class Euclidean():
	def __init__(self):
		pass
	def kernel(self, known_data, test_ins):
		scores = np.sqrt( np.sum( (known_data - test_ins.reshape((1, -1)))**2, axis=1) )
		return -1 * scores

class Cosine():
	def __init__(self):
		pass
	def kernel(self, known_data, test_ins):
		known_data = known_data / np.linalg.norm(known_data, axis=1).reshape((-1, 1))
		return (known_data @ (test_ins / np.linalg.norm(test_ins))).reshape((-1, 1)).flatten()

class RBF():
	def __init__(self, sigma=1):
		self.sigma = sigma
	def kernel(self, known_data, test_ins):
		known_data = known_data / np.linalg.norm(known_data, axis=1).reshape((-1, 1))
		test_ins = test_ins/np.linalg.norm(test_ins)
		v = (-1 / (2 * self.sigma)) * np.sum( (known_data - test_ins.reshape((1, -1)))**2, axis=1)
		return np.exp(v)

class KNN():
	def __init__(self, kernel, mode="window", k=5, radius=1):
		'''
		:param mode: Either "window" or "absolute". If window, selects all neighbors within radius.
			Otherwise picks the k nearest neighbors.
		:param kernel: The kernel function to use.
		:param k: Size of the absolute amount of kernels.
		:param radius: Radius to use for the kernel function.
		'''
		self.mode = mode
		self.kernel = kernel
		self.k = k
		self.radius = radius
	def fit(self, all_x, all_y):
		self.obs = all_x
		self.labels = all_y
	def classify(self, x):
		self._assert_fit()
		scores = self.kernel.kernel(self.obs, x)

		fallback = False
		if(self.mode == "window"):
			cand = Counter(self.labels[scores > self.radius]).most_common(1)
			if(len(cand) != 0):
				return cand[0][0]
			fallback = True
		if(fallback or self.mode == "absolute"):
			c = np.argpartition(scores, -self.k)[-1 * self.k:]
			cand = Counter(self.labels[c]).most_common(1)
			return cand[0][0]


	def batch_accuracy(self, x, y, verbose=False):
		num_correct = 0
		l = len(y)
		i = 0
		for x_ins, y_ins in zip(x, y):
			pred = self.classify(x_ins)
			if(verbose):
				print(f"\r{round(i / l, 3)}", end="")
			num_correct += int((pred == y_ins).all())
			i += 1
		print("")
		return num_correct / len(x)

	def _assert_fit(self):
		if(self.obs is None and self.labels is None):
			raise Exception("The model cannot be used for predictions without being fit first.")


class KDE():
	def __init__(self, kernel):
		'''
		:param kernel: The kernel function to use.
		'''
		self.kernel = kernel

	def fit(self, all_x, all_y):
		self.unique_labels = np.unique(all_y)
		self.label_to_idx = {l: idx for idx, l in enumerate(self.unique_labels)}
		self.sorted_labels = [all_y[all_y == l] for l in self.unique_labels]
		self.sorted_obs = [all_x[all_y == l] for l in self.unique_labels]
		self.obs = all_x
		self.labels = all_y
		self.label_likelihoods = np.array([ len(self.sorted_labels[i]) / len(all_y) for i in range(len(self.unique_labels))])

	def label_likelihood(self, x, c):
		l_idxs = [self.label_to_idx[k] for k in self.unique_labels]
		p_z_given_c = np.array([np.sum(self.kernel.kernel(self.sorted_obs[idx], x)) * 1/len(self.sorted_labels[self.label_to_idx[idx]])
			for idx in l_idxs ])
		p_z_and_c = self.label_likelihoods * p_z_given_c
		c_idx = self.label_to_idx[c]
		return p_z_and_c[c_idx] / np.sum(p_z_and_c)

	def classify(self, x):
		return max(self.unique_labels, key = lambda v: self.label_likelihood(x, v))

	def batch_accuracy(self, x, y, verbose=False):
		num_correct = 0
		l = len(y)
		i = 0
		for x_ins, y_ins in zip(x, y):
			pred = self.classify(x_ins)
			if(verbose):
				print(f"\r{round(i / l, 3)}", end="")
			num_correct += int((pred == y_ins).all())
			i += 1
		print("")
		return num_correct / len(x)



