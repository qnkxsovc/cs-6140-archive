import numpy as np 
# General Algorithm:
# Use the normal equations for fitting a linear regression to acheive maximum liklihood of observed data
# Reading says this is eq to maximizing mean squared error 

class Regularizer():
	def __init__(self):
		pass
	@staticmethod
	def _none(x, y):
		return np.linalg.pinv(x.T @ x) @ x.T @ y
	def none():
		return Regularizer._none
	def _ridge(x, y, learn_rate=0.01):
		return np.linalg.pinv(x.T @ x + learn_rate * np.identity(x.shape[1])) @ x.T @ y
	def ridge():
		return Regularizer._ridge
class RLinear():
	def __init__(self):
		pass
	@staticmethod
	def _add_bias_dim(x):
		if(len(x.shape) == 1):
			return np.concatenate(([1], x))
		else:
			return np.concatenate((np.ones((x.shape[0], 1)), x), axis=1)
	def fit(self, x_obs, y_obs, normal_eq=Regularizer.none()):
		'''
		Given the observed features and labels, fit a linear regressor to predict labels given features

		Parameters:
		----------
		x_obs: an MxN matrix of M observed events each with N features
		y_obs: a 1xM matrix of M labels for the events
		'''
		x_obs = self._add_bias_dim(x_obs)
		self.weights = normal_eq(x_obs, y_obs)
	def classify(self, x):
		try:
			return np.dot(self.weights, self._add_bias_dim(x))
		except AttributeError:
			raise Exception("The regression model must be fit to some data before it can be called")
	
	def test(self, x, y):
		# average distance from predictions to labels
		predicted = np.apply_along_axis(self.classify, 1, x)
		return np.mean(np.absolute(predicted - y))
