import math
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt 
from collections import Counter

class BinaryEvaluator():
	def __init__(self, model):
		self.model = model
	def confusion_matrix(self, eval_x, eval_y, threshold=0.5):
		eval_y = eval_y.flatten()
		mat = np.zeros((2, 2))
		results = [(self.model.label_prediction(eval_x[i], 1) > threshold) for i in range(len(eval_x))]
		for out, tar in zip(results, eval_y):
			mat[(out != tar).astype(int)][(1 - out).astype(int)] += 1
		return pd.DataFrame(mat, index=["true", "false"], columns=["pos", "neg"])
	def true_pos_rate(self, eval_x, eval_y, threshold=0.5):
		cm = self.confusion_matrix(eval_x, eval_y, threshold).to_numpy()
		return cm[0][0] / (cm[0][0] + cm[1][1])
	def false_pos_rate(self, eval_x, eval_y, threshold=0.5):
		cm = self.confusion_matrix(eval_x, eval_y, threshold).to_numpy()
		return cm[1][0] / (cm[1][0] + cm[0][1])
	def _points_on_roc(self, eval_x, eval_y):
		eval_y = eval_y.flatten()

		rate_pairs = []

		results = np.array([self.model.label_prediction(eval_x[i], 1) for i in range(len(eval_x))])
		sort_idxs = np.argsort(results)
		s_r, s_a = results[sort_idxs], eval_y[sort_idxs]

		tpr = 1
		fpr = 1
		p = len(eval_y[eval_y == 1])
		n = len(eval_y[eval_y == 0])

		counter = 1
		for i in range(1, len(s_a)):
			label = s_a[i]
			if(s_r[i] == s_r[i-1]):
				counter += 1
			else:
				if(label == 0):
					fpr -= counter/n
				if(label == 1):
					tpr -= counter/p
				counter = 1
			rate_pairs.append((fpr, tpr))
		return rate_pairs
	def show_roc(self, eval_x, eval_y):
		x, y = zip(*self._points_on_roc(eval_x, eval_y))
		plt.plot(x, y, "xb")
		plt.xlim((0, 1))
		plt.ylim((0, 1))
		plt.show()
	def auc(self, eval_x, eval_y):
		roc = self._points_on_roc(eval_x, eval_y)
		s_pairs = sorted(roc, key=lambda p: p[0], reverse=True)
		return sum( [(s_pairs[i][0] - s_pairs[i+1][0]) * s_pairs[i][1] for i in range(len(s_pairs) - 1) ] )